import { default as Client } from './Client';
export default function (baseURL?: string): Client;
