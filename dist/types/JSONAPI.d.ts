export interface Meta {
    [key: string]: any;
}
export interface ErrorObject {
    id?: string;
    links?: {
        about: string;
    };
    status?: string;
    code?: string;
    title?: string;
    detail?: string;
    source?: {
        pointer?: string;
        parameter?: string;
    };
    meta?: Meta;
}
export interface Link {
    href: string;
    meta?: Meta;
}
export interface ResourceIdentifier extends Object {
    id: string;
    type: string;
    meta?: Meta;
}
export interface ResourceObject extends ResourceIdentifier {
    attributes?: {
        [key: string]: any;
    };
    relationships?: {
        [key: string]: Relationship;
    };
    links?: {
        self?: string | Link;
    } & {
        [key: string]: string | Link;
    };
}
export interface Relationship {
    data?: ResourceIdentifier | ResourceIdentifier[] | null;
    links?: {
        self?: string | Link;
        related?: string | Link;
        first?: string | Link;
        last?: string | Link;
        next?: string | Link;
        prev?: string | Link;
    } & {
        [key: string]: string | Link;
    };
    meta?: Meta;
}
export type PrimaryData = ResourceObject | ResourceIdentifier | ResourceIdentifier[] | ResourceObject[] | null;
export interface CompoundDocument {
    jsonapi?: {
        version: string;
    };
    data?: PrimaryData;
    errors?: ErrorObject[];
    meta?: Meta;
    links?: {
        self?: string | Link;
        related?: string | Link;
        first?: string | Link;
        last?: string | Link;
        next?: string | Link;
        prev?: string | Link;
    } & {
        [key: string]: string | Link;
    };
    included?: ResourceObject[];
}
