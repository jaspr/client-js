import { BinaryExpression, Expression, ExpressionStrategy, FieldExpression, FunctionExpression, GroupExpression, LiteralExpression, UnaryExpression } from './Expression';
import { CompoundDocument, Relationship, ResourceObject, ResourceIdentifier, Meta, Link } from './JSONAPI';
import { default as useNativeFetchClient } from './NativeFetchClient';
import { default as ODataExpressionStrategy } from './ODataExpressionStrategy';
import { default as PrettyExpressionBuilder, ExpressionBuilder } from './PrettyExpressionBuilder';
import { default as QuatrodotExpressionStrategy } from './QuatrodotExpressionStrategy';
import { default as Client } from './Client';
declare type RecursivePartial<T> = {
    [P in keyof T]?: RecursivePartial<T[P]> | T[P];
};
declare type PartialResource<T extends Resource> = RecursivePartial<T>;
declare type ResourceList<T extends Index> = {
    [P in keyof T]: CollectionMethods<T[P]>;
};
declare type IndexDocument<T extends Index> = CompoundDocument & {
    links: {
        [P in keyof T]: Link | string;
    };
};
declare type SuggestRelationship<T extends Resource> = keyof T['relationships'] | string;
declare type SuggestAttribute<T extends Resource> = keyof T['attributes'] | string;
declare type SuggestedField<T extends Resource> = SuggestAttribute<T> | SuggestRelationship<T> | string;
declare type SuggestedType<T extends Resource> = T['type'] | string;
interface Filterable<T extends Resource> {
    /**
     * Adds custom parameter to query part of URL in format ?name=value
     * @param name
     * @param value
     */
    param(name: string, value: string): this;
    /**
     * Sparse fields from resource type
     * @link https://jsonapi.org/format/1.0/#fetching-sparse-fieldsets
     * @param type
     * @param fields
     */
    fields(type: SuggestedType<T>, fields: SuggestedField<T>[]): this;
    /**
     * Inclusion as defined by JSON Specification v1.0
     * @link https://jsonapi.org/format/1.0/#fetching-includes
     * @param path
     */
    include(path: SuggestRelationship<T>): this;
    /**
     * Sort by field name
     * @link https://jsonapi.org/format/1.0/#fetching-sorting
     * @param field
     * @param desc
     */
    sort(field: SuggestAttribute<T>, desc: boolean): this;
    /**
     * Offset-based strategy
     * @link https://jsonapi.org/format/1.0/#fetching-pagination
     * @param limit
     * @param offset
     */
    limitOffset(limit: number | null, offset: number | null): this;
    /**
     * Page-based strategy
     * @link https://jsonapi.org/format/1.0/#fetching-pagination
     * @param number
     * @param size
     */
    numberSize(number: number | null, size: number | null): this;
    /**
     * Filter by expression from selected strategy
     * @see ODataExpressionStrategy.ts
     * @see QuatrodotExpressionStrategy.ts
     * @param expression
     */
    filter(expression: Expression | string | null): this;
}
interface Traversable<T> {
    next(signal?: AbortSignal): Promise<T | undefined>;
    prev(signal?: AbortSignal): Promise<T | undefined>;
    first(signal?: AbortSignal): Promise<T | undefined>;
    last(signal?: AbortSignal): Promise<T | undefined>;
}
interface Resource extends ResourceObject {
    relationships?: {
        [key: string]: ToOneRelationship | ToManyRelationship;
    };
    fields(type: SuggestedType<this>, fields: SuggestedField<this>[]): this;
    include(pointer: SuggestRelationship<this>): this;
    param(name: string, value: string): this;
    self(signal?: AbortSignal): Promise<ResourceDocument<this> | undefined>;
    update(data: PartialResource<this>, signal?: AbortSignal): Promise<ResourceDocument<this> | undefined>;
    delete(signal?: AbortSignal): Promise<ResourceDocument<this> | undefined>;
}
interface ToOneRelationship<T extends Resource = Resource> extends Relationship {
    data?: (Partial<T> & ResourceIdentifier) | null;
    self(signal?: AbortSignal): Promise<IdentifierDocument | undefined>;
    update(identifier: ResourceIdentifier, signal?: AbortSignal): Promise<IdentifierDocument | undefined | null>;
    related(signal?: AbortSignal): Promise<ResourceDocument<T> | undefined>;
    fields(type: SuggestedType<T>, fields: SuggestedField<T>[]): ToOneRelationship<T>;
    include(pointer: SuggestRelationship<T>): ToOneRelationship<T>;
    param(name: string, value: string): ToOneRelationship<T>;
}
interface ToManyRelationship<T extends Resource = Resource> extends Filterable<T>, Relationship {
    data?: (Partial<T> & ResourceIdentifier)[];
    self(signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined>;
    related(signal?: AbortSignal): Promise<ResourceCollectionDocument<T> | undefined>;
    delete(identifiers: ResourceIdentifier[], signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined | null>;
    update(identifiers: ResourceIdentifier[], signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined | null>;
    create(identifiers: ResourceIdentifier[], signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined | null>;
}
interface IdentifierDocument<T extends Resource = Resource> extends CompoundDocument {
    data?: ResourceIdentifier | null;
    self(signal?: AbortSignal): Promise<IdentifierDocument | undefined>;
    related(signal?: AbortSignal): Promise<ResourceDocument<T> | undefined>;
}
interface IdentifierCollectionDocument<T extends Resource = Resource> extends CompoundDocument, Traversable<IdentifierCollectionDocument> {
    data: ResourceIdentifier[];
    self(signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined>;
    related(signal?: AbortSignal): Promise<ResourceCollectionDocument<T> | undefined>;
}
interface ResourceDocument<T extends Resource> extends CompoundDocument {
    data: T | null;
    self(signal?: AbortSignal): Promise<ResourceDocument<T> | undefined>;
}
interface ResourceCollectionDocument<T extends Resource> extends CompoundDocument, Traversable<ResourceCollectionDocument<T>> {
    data: T[];
    self(signal?: AbortSignal): Promise<ResourceCollectionDocument<T> | undefined>;
}
interface SelectCollection {
    select<T extends Resource>(collection: string | URL): CollectionMethods<T>;
}
interface CollectionMethods<T extends Resource> extends Filterable<T> {
    self(signal?: AbortSignal): Promise<ResourceCollectionDocument<T> | undefined>;
    id(id: string): ResourceMethods<T>;
    create(data: PartialResource<T>, signal?: AbortSignal): Promise<ResourceDocument<T> | undefined | null>;
}
interface ResourceMethods<T extends Resource> {
    self(signal?: AbortSignal): Promise<ResourceDocument<T> | undefined>;
}
interface Index {
    [key: string]: Resource;
}
interface Criteria<T extends Resource> extends Filterable<T> {
    toURL(): string;
}
declare const criteriaFactory: <T extends Resource>() => Criteria<T>;
declare const useJsonApiWithoutIndex: (client?: Client) => SelectCollection;
declare const useJsonApiWithIndex: <T extends Index>(index: IndexDocument<T>, client?: Client) => ResourceList<T>;
declare const fetchIndexDocument: <T extends Index>(client: Client, url: string) => Promise<IndexDocument<T>>;
export { ResourceIdentifier, ResourceObject, Relationship, CompoundDocument, Meta, Link, Index, IndexDocument, Resource, ResourceList, ToManyRelationship, ToOneRelationship, PartialResource, SelectCollection, CollectionMethods, ResourceMethods, ResourceCollectionDocument, ResourceDocument, IdentifierDocument, IdentifierCollectionDocument, RecursivePartial, useJsonApiWithoutIndex, useJsonApiWithIndex, fetchIndexDocument, Client, useNativeFetchClient, Expression, LiteralExpression, FieldExpression, BinaryExpression, FunctionExpression, GroupExpression, UnaryExpression, ExpressionStrategy, ODataExpressionStrategy, QuatrodotExpressionStrategy, ExpressionBuilder, PrettyExpressionBuilder, criteriaFactory };
