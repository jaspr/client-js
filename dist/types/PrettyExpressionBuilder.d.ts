import { Expression, ExpressionStrategy } from './Expression';
import { ResourceObject } from './index';
interface Operator<T extends ResourceObject> {
    eq(): Value<T>;
    ne(): Value<T>;
    gt(): Value<T>;
    ge(): Value<T>;
    lt(): Value<T>;
    le(): Value<T>;
    be(): Range<T>;
    in(): Value<T>;
    has(): Value<T>;
    startsWith(text: string): Compose<T>;
    endsWith(text: string): Compose<T>;
    contains(text: string): Compose<T>;
    concat(fieldOrValue: Expression): Operator<T>;
    indexOf(text: string): Operator<T>;
    add(fieldOrValue: Expression): Operator<T>;
    sub(fieldOrValue: Expression): Operator<T>;
    mul(fieldOrValue: Expression): Operator<T>;
    div(fieldOrValue: Expression): Operator<T>;
    mod(fieldOrValue: Expression): Operator<T>;
    not(field: Expression): Operator<T>;
    matchesPattern(pattern: string): Compose<T>;
    substring(length: number): Operator<T>;
    toLower(): Operator<T>;
    toUpper(): Operator<T>;
    lengthOf(): Operator<T>;
    trim(): Operator<T>;
    floor(): Operator<T>;
    ceiling(): Operator<T>;
    round(): Operator<T>;
    date(): Operator<T>;
    day(): Operator<T>;
    hour(): Operator<T>;
    minute(): Operator<T>;
    month(): Operator<T>;
    second(): Operator<T>;
    time(): Operator<T>;
    year(): Operator<T>;
    neg(): Operator<T>;
}
interface Value<T extends ResourceObject> {
    literal(value: any): Compose<T>;
    /**
     * @deprecated use disjunct()/conjunct() instead
     */
    right(expression: Expression): Compose<T>;
}
interface Range<T extends ResourceObject> {
    literal(from: string | number | Date, to: string | number | Date): Compose<T>;
}
interface Where<T extends ResourceObject> {
    field(name: keyof T["attributes"] | keyof T['relationships'] | string): Operator<T>;
}
interface Compose<T extends ResourceObject> {
    and(): Where<T>;
    or(): Where<T>;
    build(): Expression | null;
}
interface Grouping<T extends ResourceObject> {
    conjunct(...expressions: Expression[]): Compose<T> & Grouping<T>;
    disjunct(...expressions: Expression[]): Compose<T> & Grouping<T>;
}
export interface ExpressionBuilder<T extends ResourceObject> extends Where<T>, Compose<T>, Grouping<T> {
}
export default function <T extends ResourceObject>(strategy?: ExpressionStrategy): ExpressionBuilder<T>;
export {};
