function U(r = window.location.toString()) {
  const e = new Headers();
  return {
    delete(n, i, s) {
      const o = { headers: e, signal: s, method: "DELETE", body: i }, f = new URL(n, r);
      return fetch(f, o);
    },
    get(n, i) {
      const t = { headers: e, signal: i, method: "GET" }, o = new URL(n, r);
      return fetch(o, t);
    },
    patch(n, i, s) {
      const o = { headers: e, signal: s, method: "PATCH", body: i }, f = new URL(n, r);
      return fetch(f, o);
    },
    post(n, i, s) {
      const o = { headers: e, signal: s, method: "POST", body: i }, f = new URL(n, r);
      return fetch(f, o);
    },
    setHeader(n, i) {
      e.set(n, i);
    }
  };
}
var u = /* @__PURE__ */ ((r) => (r.LOGICAL_EQUAL = "eq", r.LOGICAL_NOT_EQUAL = "ne", r.LOGICAL_GREATER_THAN = "gt", r.LOGICAL_GREATER_THAN_OR_EQUAL = "ge", r.LOGICAL_LOWER_THAN = "lt", r.LOGICAL_LOWER_THAN_OR_EQUAL = "le", r.LOGICAL_AND = "and", r.LOGICAL_OR = "or", r.LOGICAL_NOT = "not", r.LOGICAL_HAS = "has", r.LOGICAL_IN = "in", r.LOGICAL_BETWEEN = "be", r.ARITHMETIC_ADDITION = "add", r.ARITHMETIC_SUBTRACTION = "sub", r.ARITHMETIC_NEGATION = "-", r.ARITHMETIC_MULTIPLICATION = "mul", r.ARITHMETIC_DIVISION = "div", r.ARITHMETIC_MODULO = "mod", r.FUNCTION_STARTS_WITH = "startsWith", r.FUNCTION_ENDS_WITH = "endsWith", r.FUNCTION_CONTAINS = "contains", r.FUNCTION_CONCAT = "concat", r.FUNCTION_INDEX_OF = "indexof", r.FUNCTION_LENGTH = "length", r.FUNCTION_SUBSTRING = "substring", r.FUNCTION_MATCHES_PATTERN = "matchesPattern", r.FUNCTION_TO_LOWER = "tolower", r.FUNCTION_TO_UPPER = "toupper", r.FUNCTION_TRIM = "trim", r.FUNCTION_ANY = "any", r.FUNCTION_ALL = "all", r.FUNCTION_DATE = "date", r.FUNCTION_DAY = "day", r.FUNCTION_HOUR = "hour", r.FUNCTION_MINUTE = "minute", r.FUNCTION_MONTH = "month", r.FUNCTION_NOW = "now", r.FUNCTION_SECOND = "second", r.FUNCTION_TIME = "time", r.FUNCTION_YEAR = "year", r.FUNCTION_CEILING = "ceiling", r.FUNCTION_FLOOR = "floor", r.FUNCTION_ROUND = "round", r))(u || {});
const R = function(r) {
  return {
    express() {
      return r;
    }
  };
}, L = function(r) {
  let e;
  if (r === void 0)
    throw new Error("Expected value to by one of [null, Date, String, Number, Boolean, Array], but got " + typeof r);
  if (r === null)
    e = "null";
  else if (typeof r.express == "function")
    e = r.express();
  else if (r instanceof Date)
    e = `datetime'${r.toISOString()}'`;
  else if (r instanceof Array)
    r = r.map((n) => L(n).express()), e = `(${r.join(",")})`;
  else if (typeof r == "string")
    e = `'${r.replace("'", "''")}'`;
  else if (typeof r == "boolean")
    e = r ? "true" : "false";
  else if (typeof r == "number")
    e = r.toString();
  else
    throw new Error("Expected value to by one of [null, Date, String, Number, Boolean, Array], but got " + typeof r);
  return {
    express() {
      return e;
    }
  };
}, g = function(r, e) {
  if (e !== u.LOGICAL_AND && e !== u.LOGICAL_OR)
    throw Error("Invalid group operator. Possible values are: [AND, OR].");
  return {
    addExpression(n) {
      return r.push(n), this;
    },
    express() {
      return "(" + r.map((n) => n.express()).join(` ${e} `) + ")";
    }
  };
}, F = function(r, e, n) {
  return {
    express() {
      return `${r.express()} ${e} ${n.express()}`;
    }
  };
}, $ = function(r, e) {
  return {
    express() {
      let n = `${r}(`;
      if (e.length > 0) {
        let i = !1;
        for (const s of e)
          i && (n += ","), n += `${s.express()}`, i = !0;
      }
      return n += ")", n;
    }
  };
}, w = function(r, e) {
  return {
    express() {
      return `${e} ${r.express()}`;
    }
  };
}, S = {
  literal: L,
  field: R,
  groupExpression: g,
  binaryExpression: F,
  functionExpression: $,
  unaryExpression: w
};
function P(r) {
  const e = r ?? S;
  let n = null, i = null, s, t, o, f;
  const l = {
    and() {
      return i = this.build(), s = u.LOGICAL_AND, n = null, O;
    },
    or() {
      return i = this.build(), s = u.LOGICAL_OR, n = null, O;
    },
    build() {
      if (!n)
        if (t && o && f)
          n = e.binaryExpression(t, o, f);
        else
          return null;
      return i && (n = e.binaryExpression(i, s, n)), n;
    }
  }, a = {
    right(c) {
      return f = c, l;
    },
    literal(c) {
      return f = e.literal(c), l;
    }
  }, N = {
    literal(c, y) {
      return f = e.literal([c, y]), l;
    }
  }, T = {
    add(c) {
      return t = e.binaryExpression(t, u.ARITHMETIC_ADDITION, c), this;
    },
    ceiling() {
      return t = e.functionExpression(u.FUNCTION_CEILING, [t]), this;
    },
    concat(c) {
      return t = e.functionExpression(u.FUNCTION_CONCAT, [t, c]), this;
    },
    date() {
      return t = e.functionExpression(u.FUNCTION_DATE, [t]), this;
    },
    day() {
      return t = e.functionExpression(u.FUNCTION_DAY, [t]), this;
    },
    div(c) {
      return t = e.binaryExpression(t, u.ARITHMETIC_DIVISION, c), this;
    },
    floor() {
      return t = e.functionExpression(u.FUNCTION_FLOOR, [t]), this;
    },
    has() {
      return o = u.LOGICAL_HAS, a;
    },
    hour() {
      return t = e.functionExpression(u.FUNCTION_HOUR, [t]), this;
    },
    indexOf(c) {
      return t = e.functionExpression(u.FUNCTION_INDEX_OF, [t, e.literal(c)]), this;
    },
    lengthOf() {
      return t = e.functionExpression(u.FUNCTION_LENGTH, [t]), this;
    },
    matchesPattern(c) {
      return n = e.functionExpression(u.FUNCTION_MATCHES_PATTERN, [t, e.literal(c)]), l;
    },
    minute() {
      return t = e.functionExpression(u.FUNCTION_MINUTE, [t]), this;
    },
    mod(c) {
      return t = e.binaryExpression(t, u.ARITHMETIC_MODULO, c), this;
    },
    month() {
      return t = e.functionExpression(u.FUNCTION_MONTH, [t]), this;
    },
    mul(c) {
      return t = e.binaryExpression(t, u.ARITHMETIC_MULTIPLICATION, c), this;
    },
    neg() {
      return t = e.unaryExpression(t, u.ARITHMETIC_NEGATION), this;
    },
    not() {
      return t = e.unaryExpression(t, u.LOGICAL_NOT), this;
    },
    round() {
      return t = e.functionExpression(u.FUNCTION_ROUND, [t]), this;
    },
    second() {
      return t = e.functionExpression(u.FUNCTION_SECOND, [t]), this;
    },
    sub(c) {
      return t = e.binaryExpression(t, u.ARITHMETIC_SUBTRACTION, c), this;
    },
    substring(c) {
      return t = e.functionExpression(u.FUNCTION_SUBSTRING, [t, e.literal(c)]), this;
    },
    time() {
      return t = e.functionExpression(u.FUNCTION_SECOND, [t]), this;
    },
    toLower() {
      return t = e.functionExpression(u.FUNCTION_TO_LOWER, [t]), this;
    },
    toUpper() {
      return t = e.functionExpression(u.FUNCTION_TO_UPPER, [t]), this;
    },
    trim() {
      return t = e.functionExpression(u.FUNCTION_TRIM, [t]), this;
    },
    year() {
      return t = e.functionExpression(u.FUNCTION_SECOND, [t]), this;
    },
    be() {
      return o = u.LOGICAL_BETWEEN, N;
    },
    contains(c) {
      return n = e.functionExpression(u.FUNCTION_CONTAINS, [t, e.literal(c)]), l;
    },
    endsWith(c) {
      return n = e.functionExpression(u.FUNCTION_ENDS_WITH, [t, e.literal(c)]), l;
    },
    eq() {
      return o = u.LOGICAL_EQUAL, a;
    },
    ge() {
      return o = u.LOGICAL_GREATER_THAN_OR_EQUAL, a;
    },
    gt() {
      return o = u.LOGICAL_GREATER_THAN, a;
    },
    in() {
      return o = u.LOGICAL_IN, a;
    },
    le() {
      return o = u.LOGICAL_LOWER_THAN_OR_EQUAL, a;
    },
    lt() {
      return o = u.LOGICAL_LOWER_THAN, a;
    },
    ne() {
      return o = u.LOGICAL_NOT_EQUAL, a;
    },
    startsWith(c) {
      return n = e.functionExpression(u.FUNCTION_STARTS_WITH, [t, e.literal(c)]), l;
    }
  }, O = {
    field(c) {
      return t = e.field(c), T;
    }
  }, _ = {
    conjunct(...c) {
      return n = e.groupExpression(c, u.LOGICAL_AND), { ...l, ..._ };
    },
    disjunct(...c) {
      return n = e.groupExpression(c, u.LOGICAL_OR), { ...l, ..._ };
    }
  };
  return { ...O, ...l, ..._ };
}
const H = [
  u.FUNCTION_CONCAT,
  u.FUNCTION_INDEX_OF,
  u.FUNCTION_LENGTH,
  u.FUNCTION_SUBSTRING,
  u.ARITHMETIC_ADDITION,
  u.ARITHMETIC_SUBTRACTION,
  u.ARITHMETIC_NEGATION,
  u.ARITHMETIC_MULTIPLICATION,
  u.ARITHMETIC_DIVISION,
  u.ARITHMETIC_MODULO,
  u.FUNCTION_MATCHES_PATTERN,
  u.FUNCTION_TO_LOWER,
  u.FUNCTION_TO_UPPER,
  u.FUNCTION_TRIM,
  u.FUNCTION_DATE,
  u.FUNCTION_DAY,
  u.FUNCTION_HOUR,
  u.FUNCTION_MINUTE,
  u.FUNCTION_MONTH,
  u.FUNCTION_NOW,
  u.FUNCTION_SECOND,
  u.FUNCTION_TIME,
  u.FUNCTION_YEAR,
  u.FUNCTION_CEILING,
  u.FUNCTION_FLOOR,
  u.FUNCTION_ROUND
], C = (r) => {
  if (H.includes(r))
    throw new Error(`Unsupported operator ${r}.`);
}, b = function(r) {
  return {
    express() {
      return r;
    }
  };
}, x = function(r) {
  let e;
  if (r === void 0)
    throw new Error("Expected value to by one of [null, Date, String, Number, Boolean, Array], but got " + typeof r);
  if (r === null)
    e = "null";
  else if (typeof r.express == "function")
    e = r.express();
  else if (r instanceof Date)
    e = `${r.toISOString()}`;
  else if (r instanceof Array)
    r = r.map((n) => x(n).express()), e = `${r.join("::")}`;
  else if (typeof r == "string")
    e = `${r}`;
  else if (typeof r == "boolean")
    e = r ? "true" : "false";
  else if (typeof r == "number")
    e = r.toString();
  else
    throw new Error("Expected value to by one of [null, Date, String, Number, Boolean, Array], but got " + typeof r);
  return {
    express: function() {
      return e;
    }
  };
}, G = function(r = []) {
  let e = r;
  return {
    addExpression(n) {
      return e.push(n), this;
    },
    express() {
      return `${e.map((n) => n.express()).join("|")}`;
    }
  };
}, D = function(r, e, n) {
  return C(e), {
    express() {
      return [u.LOGICAL_AND, u.LOGICAL_OR].includes(e) ? `${r.express()}|${n.express()}` : `${r.express()}::${e}::${n.express()}`;
    }
  };
}, m = function(r, e) {
  C(r);
  const n = e[0], i = e[1];
  return {
    express() {
      return `${n.express()}::${r}::${i.express()}`;
    }
  };
}, j = function(r, e) {
  throw new Error("Unsupported expression.");
}, W = {
  field: b,
  literal: x,
  unaryExpression: j,
  groupExpression: G,
  binaryExpression: D,
  functionExpression: m
}, E = (r) => JSON.stringify({
  jsonapi: { version: "1.0" },
  data: r
}), p = () => {
  let r = {}, e = null, n = null, i = null, s = null, t = [], o = null, f = {}, l = {};
  return {
    param(a, N) {
      return l[a] = N, this;
    },
    fields(a, N) {
      return r[a] = N.concat(r[a] || []), this;
    },
    include(a) {
      return t.push(a), this;
    },
    sort(a, N = !1) {
      return f[a] = N, this;
    },
    limitOffset(a, N) {
      return e = a, n = N, this;
    },
    numberSize(a, N) {
      return i = a, s = N, this;
    },
    filter(a) {
      return o = a, this;
    },
    toURL() {
      let a = "", N = "?";
      if ((i !== null || s !== null) && (e !== null || n !== null))
        throw new Error("PageSize strategy and LimitOffset strategy cannot be used simultaneously.");
      e !== null && (a += N + "page[limit]=" + e.toString(), N = "&"), n !== null && (a += N + "page[offset]=" + n.toString(), N = "&"), i !== null && (a += N + "page[number]=" + i.toString(), N = "&"), s !== null && (a += N + "page[size]=" + s.toString(), N = "&");
      for (const O in r)
        a += N + `fields[${O}]=` + r[O].join(), N = "&";
      o && (a += N + "filter=" + (typeof o == "string" ? o : o.express()), N = "&"), t.length > 0 && (a += N + "include=" + t.join(), N = "&");
      const T = [];
      for (const O in f) {
        const _ = f[O];
        T.push(`${_ ? "-" : ""}${O}`);
      }
      T.length && (a += N + "sort=" + T.join(","), N = "&");
      for (const O in l)
        a += N + `${[O]}=${l[O]}`, N = "&";
      return a;
    }
  };
}, M = (r, e) => {
  let n = p(), i = r.data;
  return {
    ...r,
    data: i,
    fields(s, t) {
      return n.fields(s, t), this;
    },
    filter(s = null) {
      return n.filter(s), this;
    },
    include(s) {
      return n.include(s), this;
    },
    limitOffset(s, t) {
      return n.limitOffset(s, t), this;
    },
    numberSize(s, t) {
      return n.numberSize(s, t), this;
    },
    sort(s, t = !0) {
      return n.sort(s, t), this;
    },
    param(s, t) {
      return n.param(s, t), this;
    },
    async self(s) {
      var t;
      if ((t = r.links) != null && t.self) {
        const o = `${r.links.self}${n.toURL()}`, l = await (await e.get(o, s)).json();
        return n = p(), I(l, e);
      }
      n = p();
    },
    async related(s) {
      var t;
      if ((t = r.links) != null && t.related) {
        const o = `${r.links.related}${n.toURL()}`, f = await e.get(o, s);
        n = p();
        const l = await f.json();
        return I(l, e);
      }
    },
    async update(s, t) {
      var o;
      if ((o = r.links) != null && o.self) {
        const f = `${r.links.self}${n.toURL()}`, l = await e.patch(f, E(s), t);
        if (n = p(), l.status === 204)
          return null;
        const a = await l.json();
        return I(a, e);
      }
    },
    async create(s, t) {
      var o;
      if ((o = r.links) != null && o.self) {
        const f = `${r.links.self}${n.toURL()}`, l = await e.post(f, E(s), t);
        if (n = p(), l.status === 204)
          return null;
        const a = await l.json();
        return I(a, e);
      }
    },
    async delete(s, t) {
      var o;
      if ((o = r.links) != null && o.self) {
        const f = `${r.links.self}${n.toURL()}`, l = await e.delete(f, E(s), t);
        if (n = p(), l.status === 204)
          return null;
        const a = await l.json();
        return I(a, e);
      }
    }
  };
}, h = (r, e) => {
  var s;
  let n = p();
  const i = "links" in r && ((s = r.links) != null && s.self) ? r.links.self : void 0;
  if ("relationships" in r)
    for (const t in r.relationships)
      r.relationships[t] = M(r.relationships[t], e);
  return {
    ...r,
    fields(t, o) {
      return n.fields(t, o), this;
    },
    include(t) {
      return n.include(t), this;
    },
    param(t, o) {
      return n.param(t, o), this;
    },
    async self(t) {
      if (i) {
        const o = `${i}${n.toURL()}`, l = await (await e.get(o, t)).json();
        return n = p(), I(l, e);
      }
      n = p();
    },
    async update(t, o) {
      if (i) {
        const f = `${i}${n.toURL()}`, l = await e.patch(f, E(t), o);
        if (n = p(), l.status === 204)
          return null;
        const a = await l.json();
        return I(a, e);
      }
      n = p();
    },
    async delete(t) {
      if (i) {
        const o = `${i}${n.toURL()}`, f = await e.delete(o, void 0, t);
        if (n = p(), f.status === 204)
          return null;
        const l = await f.json();
        return I(l, e);
      }
      n = p();
    }
  };
}, A = (r, e) => r == null ? r : Array.isArray(r) ? r.map((n) => h(n, e)) : h(r, e), d = (r, e) => {
  if (r && !Object.hasOwn(r, "hydrated") && (Object.defineProperty(r, "hydrated", { value: !0 }), r.relationships)) {
    for (const n of Object.values(r.relationships))
      if (Array.isArray(n.data))
        n.data = n.data.map((i) => {
          const s = `${i.type}:${i.id}`;
          if (e.has(s)) {
            const t = e.get(s);
            return t.relationships && d(t, e), t;
          } else
            return i;
        });
      else if (n.data) {
        const i = n.data, s = `${i.type}:${i.id}`;
        if (e.has(s)) {
          const t = e.get(s);
          t.relationships && d(t, e), n.data = t;
        }
      }
  }
}, I = (r, e) => {
  if (r.errors)
    throw r;
  const n = /* @__PURE__ */ new Map();
  r.included !== void 0 && r.included.forEach((s) => {
    const t = A(s, e), o = `${s.type}:${s.id}`;
    n.set(o, t);
  });
  let i;
  if (Array.isArray(r.data))
    i = r.data.map((s) => {
      const t = A(s, e), o = `${s.type}:${s.id}`;
      return n.set(o, t), t;
    }), i.forEach((s) => {
      d(s, n);
    });
  else {
    i = A(r.data, e);
    const s = `${i.type}:${i.id}`;
    n.set(s, i), d(i, n);
  }
  return {
    ...r,
    // <-- This! Right here, is cancer.
    data: i,
    async self(s) {
      var t;
      if ((t = r.links) != null && t.self) {
        const o = `${r.links.self}`, l = await (await e.get(o, s)).json();
        return I(l, e);
      }
    },
    async related(s) {
      var t;
      if ((t = r.links) != null && t.related) {
        const o = `${r.links.related}`, l = await (await e.get(o, s)).json();
        return I(l, e);
      }
    },
    async first(s) {
      var t;
      if ((t = r.links) != null && t.first) {
        const o = `${r.links.self}`, l = await (await e.get(o, s)).json();
        return I(l, e);
      }
    },
    async last(s) {
      var t;
      if ((t = r.links) != null && t.last) {
        const o = `${r.links.last}`, l = await (await e.get(o, s)).json();
        return I(l, e);
      }
    },
    async next(s) {
      var t;
      if ((t = r.links) != null && t.next) {
        const o = `${r.links.next}`, l = await (await e.get(o, s)).json();
        return I(l, e);
      }
    },
    async prev(s) {
      var t;
      if ((t = r.links) != null && t.prev) {
        const o = `${r.links.prev}`, l = await (await e.get(o, s)).json();
        return I(l, e);
      }
    }
  };
}, k = function(r = U()) {
  return r.setHeader("Accept", "application/vnd.api+json"), r.setHeader("Content-Type", "application/vnd.api+json"), {
    select(e) {
      let n = p();
      return {
        async create(i, s) {
          const t = `${e}${n.toURL()}`, o = await r.post(t, E(i), s);
          if (n = p(), o.status === 204)
            return null;
          const f = await o.json();
          return I(f, r);
        },
        fields(i, s) {
          return n.fields(i, s), this;
        },
        filter(i = null) {
          return n.filter(i), this;
        },
        id(i) {
          return {
            async self(s) {
              const t = `${e}/${i}${n.toURL()}`, o = await r.get(t, s);
              n = p();
              const f = await o.json();
              return I(f, r);
            }
          };
        },
        include(i) {
          return n.include(i), this;
        },
        limitOffset(i, s) {
          return n.limitOffset(i, s), this;
        },
        numberSize(i, s) {
          return n.numberSize(i, s), this;
        },
        sort(i, s = !0) {
          return n.sort(i, s), this;
        },
        param(i, s) {
          return n.param(i, s), this;
        },
        async self(i) {
          const s = `${e}${n.toURL()}`, t = await r.get(s, i);
          n = p();
          const o = await t.json();
          return I(o, r);
        }
      };
    }
  };
}, B = function(r, e = U()) {
  const n = {};
  for (const i in r.links) {
    const s = r.links[i];
    if (s) {
      let t;
      typeof s != "string" ? t = s.href : t = s;
      const { select: o } = k(e);
      Object.defineProperty(n, i, {
        get: () => o(t),
        configurable: !0
      });
    }
  }
  return n;
}, v = async (r, e) => {
  r.setHeader("Accept", "application/vnd.api+json"), r.setHeader("Content-Type", "application/vnd.api+json");
  const i = await (await r.get(e)).json();
  if (i.errors)
    throw i;
  return i;
};
export {
  S as ODataExpressionStrategy,
  P as PrettyExpressionBuilder,
  W as QuatrodotExpressionStrategy,
  p as criteriaFactory,
  v as fetchIndexDocument,
  B as useJsonApiWithIndex,
  k as useJsonApiWithoutIndex,
  U as useNativeFetchClient
};
//# sourceMappingURL=client-js.js.map
