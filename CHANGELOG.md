# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [5.2.0](https://gitlab.com/jaspr/client-js/compare/5.1.0...5.2.0) (2025-02-21)


### Added

* advanced type-hinting ([81ede92](https://gitlab.com/jaspr/client-js/commit/81ede923a0103382f747a655d847c36ba9d7964b))
* inclusion relationship data mapping ([b91d98b](https://gitlab.com/jaspr/client-js/commit/b91d98b3d9a276d1d64c4d66fb62af7fc1666cec))


### Fixed

* recursive inclusion ([edfb948](https://gitlab.com/jaspr/client-js/commit/edfb948886622c0f9ccb1b8c1d61aa7f3929721a))
* typings ([278d03c](https://gitlab.com/jaspr/client-js/commit/278d03c339f2914f7d1892087a5d88296c893565))
* undefined data in relationship ([a4938e4](https://gitlab.com/jaspr/client-js/commit/a4938e4dd012d0184f2325ef76d0bf225c58418d))


### Changed

* relationship data typings ([5817efd](https://gitlab.com/jaspr/client-js/commit/5817efdcf19a6d3fc23ac06a18ccdbacb0e66083))

## [5.1.0](https://gitlab.com/jaspr/client-js/compare/5.0.1...5.1.0) (2024-11-25)


### Added

* export interface RecursivePartial ([27113df](https://gitlab.com/jaspr/client-js/commit/27113dfb11c0d65ca0396617e4c7a4ae1d02e6cf))

### [5.0.1](https://gitlab.com/jaspr/client-js/compare/5.0.0...5.0.1) (2024-05-31)


### Fixed

* reset criteria after request is done ([3d241ca](https://gitlab.com/jaspr/client-js/commit/3d241ca26cd73dcecf487b502b6bb1fe5d50cf31))

## [5.0.0](https://gitlab.com/jaspr/client-js/compare/4.2.4...5.0.0) (2024-05-11)


### ⚠ BREAKING CHANGES

* Replace methods limit, offset to limitOffset
* Change return type of methods

### Fixed

* Add missing optional links ([eb6985f](https://gitlab.com/jaspr/client-js/commit/eb6985f0b349022437743dd2d4076c2cc06bdadd))


### Changed

* Return null in case of empty body, undefined in case of missing link ([b7c6f6e](https://gitlab.com/jaspr/client-js/commit/b7c6f6e17ad0c4e2e5b56c6cdf6a59de856be0ac))


### Added

* Add size-based pagination strategy ([cde8ff5](https://gitlab.com/jaspr/client-js/commit/cde8ff5cff87a87b5244a85448565e2150606a3b))
* Export criteriaFactory ([c93169f](https://gitlab.com/jaspr/client-js/commit/c93169fd1e5507d05ef7f2b70dd6ad117fcef950)), closes [#3](https://gitlab.com/jaspr/client-js/issues/3)

### [4.2.4](https://gitlab.com/jaspr/client-js/compare/4.2.3...4.2.4) (2024-03-07)


### Fixed

* ResourceDocument data can be null ([934df0a](https://gitlab.com/jaspr/client-js/commit/934df0a3ab8193b66d161b2e804ebba371812dd9))

### [4.2.3](https://gitlab.com/jaspr/client-js/compare/4.2.2...4.2.3) (2024-01-05)

### [4.2.2](https://gitlab.com/jaspr/client-js/compare/4.2.1...4.2.2) (2024-01-05)

### [4.2.1](https://gitlab.com/jaspr/client-js/compare/4.2.0...4.2.1) (2023-11-29)


### Fixed

* remove extra char at url expand ([667b3c7](https://gitlab.com/jaspr/client-js/commit/667b3c733ff978575dc21334ca972797dbc0cc83))

## [4.2.0](https://gitlab.com/jaspr/client-js/compare/4.1.3...4.2.0) (2023-11-27)


### Added

* Add ExpressionBuilder interface ([c62e9b2](https://gitlab.com/jaspr/client-js/commit/c62e9b282bc1b48f6d1a8c22af5396bceec1838d))

### [4.1.3](https://gitlab.com/jaspr/client-js/compare/4.1.2...4.1.3) (2023-11-27)


### Fixed

* Range literal should accept Date object ([b28dfc5](https://gitlab.com/jaspr/client-js/commit/b28dfc5fb75224463401de27c5da529568502050))

### [4.1.2](https://gitlab.com/jaspr/client-js/compare/4.1.1...4.1.2) (2023-11-07)


### Fixed

* Return undefined if HTTP Status No Content is returned from server ([f06f4eb](https://gitlab.com/jaspr/client-js/commit/f06f4eb66d2fcefe6814cd20eabc18c36fe6679a))

### [4.1.1](https://gitlab.com/jaspr/client-js/compare/4.1.0...4.1.1) (2023-11-01)


### Fixed

* export missing interface ([2fba85f](https://gitlab.com/jaspr/client-js/commit/2fba85f98d2d7f9e68696e1c832450719fa778bc))

## [4.1.0](https://gitlab.com/jaspr/client-js/compare/4.0.0...4.1.0) (2023-11-01)


### Changed

* move to Vite bundler ([440ba27](https://gitlab.com/jaspr/client-js/commit/440ba274f4709c8514583584bf4d96f1ba9b5e97))
* move to Vitest ([f28ad8a](https://gitlab.com/jaspr/client-js/commit/f28ad8a2e90dd8eaefbae377541a0958afc70b8c))


### Added

* allow string to Criteria::filter method ([58e88e1](https://gitlab.com/jaspr/client-js/commit/58e88e1b39132334e74ea3f8fe58d9320e1e93ce))

## [4.0.0](https://gitlab.com/jaspr/client-js/compare/3.1.1...4.0.0) (2023-10-29)


### ⚠ BREAKING CHANGES

* * useNativeFetchClient
* useJsonApiWithIndex
* useJsonApiWithoutIndex

### Changed

* Relative links support ([60c641f](https://gitlab.com/jaspr/client-js/commit/60c641f99f929526dc369f18c4a08301390c3e8f))

### [3.1.1](https://gitlab.com/jaspr/client-js/compare/3.1.0...3.1.1) (2023-09-27)


### Fixed

* add implicit identification ([e1592c6](https://gitlab.com/jaspr/client-js/commit/e1592c60c6c9d976dbfc9e3ebb58d3b7c8fe83ef))
* add missing interface to export ([85a65ba](https://gitlab.com/jaspr/client-js/commit/85a65ba452d917726d90e4ed37adbc1f759cea1c))
* missing type passing ([f56b6e2](https://gitlab.com/jaspr/client-js/commit/f56b6e23b13e23801ff5d5f00855e2a9b3f04cc9))

## [3.1.0](https://gitlab.com/jaspr/client-js/compare/3.0.6...3.1.0) (2023-09-07)


### Added

* add type support to ToOneRelationship, ToManyRelationship ([18767bd](https://gitlab.com/jaspr/client-js/commit/18767bd6dae38c07ec9b7b0d09dd316edb948b1c))

### [3.0.6](https://gitlab.com/jaspr/client-js/compare/3.0.5...3.0.6) (2023-07-21)


### Fixed

* Global client got override ([516d0eb](https://gitlab.com/jaspr/client-js/commit/516d0eb19266841518dac9be4cd9e371977186bb))

### [3.0.5](https://gitlab.com/jaspr/client-js/compare/3.0.4...3.0.5) (2023-07-17)


### Fixed

* use proper type ([d5a1d92](https://gitlab.com/jaspr/client-js/commit/d5a1d92c44bb36a2808404a3fe5f3c7e7fcc887c))

### [3.0.4](https://gitlab.com/jaspr/client-js/compare/3.0.3...3.0.4) (2023-06-23)


### Fixed

* a little rework of NativeFetchClient ([771bcf3](https://gitlab.com/jaspr/client-js/commit/771bcf3d65643d5ba33dd7a3fc570c93b8e36b49))

### [3.0.3](https://gitlab.com/jaspr/client-js/compare/3.0.2...3.0.3) (2023-06-21)


### Fixed

* add missing export ([e77b8fd](https://gitlab.com/jaspr/client-js/commit/e77b8fd85467d706a9814fe34c27e7ab6acb6fa1)), closes [#8](https://gitlab.com/jaspr/client-js/issues/8)

### [3.0.2](https://gitlab.com/jaspr/client-js/compare/3.0.1...3.0.2) (2023-06-19)


### Fixed

* Fix typings ([7af5142](https://gitlab.com/jaspr/client-js/commit/7af51428305a333910fb901ee0c4eaa534ed0806)), closes [#7](https://gitlab.com/jaspr/client-js/issues/7) [#7](https://gitlab.com/jaspr/client-js/issues/7)

### [3.0.1](https://gitlab.com/jaspr/client-js/compare/3.0.0...3.0.1) (2023-05-17)


### Fixed

* criteria collision ([23715ee](https://gitlab.com/jaspr/client-js/commit/23715ee627287d1826ccaa71daa1a45da9030eae))

## [3.0.0](https://gitlab.com/jaspr/client-js/compare/2.4.0...3.0.0) (2023-05-14)


### ⚠ BREAKING CHANGES

* Removed default exported methods, replaces with
non-default useJsonApiWithoutIndex
* Remove deprecated methods
* Change of ::update() ::create() interfaces

### Changed

* remove deprecated methods ([ff2b0c9](https://gitlab.com/jaspr/client-js/commit/ff2b0c9d759c37fd75569a66f10fae4b8d809b9a))


### Added

* Add new JSON:API client factory method ([c06fb0f](https://gitlab.com/jaspr/client-js/commit/c06fb0fbd2393b351b862015547c819bb47218c5))
* New type PartialResource ([9d44d65](https://gitlab.com/jaspr/client-js/commit/9d44d65eba11d8de5e450357d6d6df8721342527))


### Fixed

* add missing optional arg ([151692b](https://gitlab.com/jaspr/client-js/commit/151692bfac53c9adc17c50c58eb3d8269d641604))
* Allow additional links ([1df6add](https://gitlab.com/jaspr/client-js/commit/1df6addc2fd0cafa2fd03c25698cddcec7410555))
* data can be undefined ([846a11f](https://gitlab.com/jaspr/client-js/commit/846a11f2c27a1e2540441991a26868668d681420))
* Fix factory useJsonApiWithIndex ([58ee296](https://gitlab.com/jaspr/client-js/commit/58ee296a471cf57dc2d001b1379946993872da8c))
* relationship data can be undefined ([6e37c4e](https://gitlab.com/jaspr/client-js/commit/6e37c4e8d034200dbbb086317959658511d5724f))

## [2.4.0](https://gitlab.com/jaspr/client-js/compare/2.3.0...2.4.0) (2023-03-26)


### Fixed

* handle relationships without data ([a70b31e](https://gitlab.com/jaspr/client-js/commit/a70b31ee3f72e977de9750dd1ee189140c4868ab))


### Added

* Add methods to ResourceProxy ([cf0be27](https://gitlab.com/jaspr/client-js/commit/cf0be27b93340fc4d4fe7300403b4449c2574c33)), closes [#5](https://gitlab.com/jaspr/client-js/issues/5)

## [2.3.0](https://gitlab.com/jaspr/client-js/compare/2.2.1...2.3.0) (2023-03-08)


### Added

* Add ::param() ([b08eee1](https://gitlab.com/jaspr/client-js/commit/b08eee148f735549ac152b6e77b3bee92a2ebcf9))

### [2.2.1](https://gitlab.com/jaspr/client-js/compare/2.2.0...2.2.1) (2023-02-20)


### Fixed

* change document data type ([31c6f76](https://gitlab.com/jaspr/client-js/commit/31c6f760ba8247527696ed9a7156b447da51d6ed))

## [2.2.0](https://gitlab.com/jaspr/client-js/compare/2.1.0...2.2.0) (2023-02-20)


### Added

* add grouping ([dfe4cfd](https://gitlab.com/jaspr/client-js/commit/dfe4cfd88dcdc18a7e304a2c6358d9fe0b858334))


### Changed

* remove deprecated files ([b07221a](https://gitlab.com/jaspr/client-js/commit/b07221aaa8aa4ab5b76ffa91b4860b0f206863f3))

## [2.1.0](https://gitlab.com/jaspr/client-js/compare/2.0.4...2.1.0) (2023-02-01)


### Added

* add ResourceObject type ([38c871c](https://gitlab.com/jaspr/client-js/commit/38c871c6fa542bb2b9980518ed895a5994fe49ed))


### Fixed

* Fix abort when multiple request ([ac69bd1](https://gitlab.com/jaspr/client-js/commit/ac69bd1d6904435728e2e13e3ea7a77146bcc2b1))
* typings ([90f4c8d](https://gitlab.com/jaspr/client-js/commit/90f4c8ddd8035fe02736cb244cfc94e5be577cb3))

### [2.0.4](https://gitlab.com/jaspr/client-js/compare/2.0.3...2.0.4) (2023-01-19)


### Fixed

* missing typing override ([2cf9b1c](https://gitlab.com/jaspr/client-js/commit/2cf9b1ce654dfc442471b74d0430f09732af0eec))

### [2.0.3](https://gitlab.com/jaspr/client-js/compare/2.0.2...2.0.3) (2023-01-18)


### Fixed

* refactor interfaces ([5c930fa](https://gitlab.com/jaspr/client-js/commit/5c930fa8b6cc86a82ad121eb2208f14151f6e2f7))

### [2.0.2](https://gitlab.com/jaspr/client-js/compare/2.0.1...2.0.2) (2023-01-18)


### Fixed

* export interfaces ([78c7522](https://gitlab.com/jaspr/client-js/commit/78c7522396b8e994ffdc3fef4585188a45473b45))

### [2.0.1](https://gitlab.com/jaspr/client-js/compare/2.0.0...2.0.1) (2023-01-18)

## [2.0.0](https://gitlab.com/jaspr/client-js/compare/1.0.1...2.0.0) (2022-10-29)


### ⚠ BREAKING CHANGES

* Complete rework of expression building strategy
* PrettyExpressionBuilder changed
* ExpressionBuilder renamed to ODataExpressionBuilder
* Complete rework of fetching document see [tests](tests) for examples

### Added

* Add QuatrodotExpressionBuilder filter ([a89b9aa](https://gitlab.com/jaspr/client-js/commit/a89b9aad6d5190664a885573829b4f6d985594d7))

### [1.0.1](https://gitlab.com/jaspr/client-js/compare/1.0.0...1.0.1) (2022-07-11)


### Fixed

* duplicate query part ([14ea138](https://gitlab.com/jaspr/client-js/commit/14ea1386fba2b3988efdcb4fcbfb9a4e87c9b5e3)), closes [#1](https://gitlab.com/jaspr/client-js/issues/1)

## 1.0.0 (2022-06-20)


### Changed

* moving project ([97b955f](https://gitlab.com/jaspr/client-js/commit/97b955f60577e7b505b952d8504b78d5dae96c92))
* rename project namespace ([89c1867](https://gitlab.com/jaspr/client-js/commit/89c1867f0fec356978a3820e9093e4a95e70b037))
