import { expect, test, describe } from 'vitest'
import {HOST, TestResource} from "./Server";
import {fetchIndexDocument, Index, useJsonApiWithIndex, useNativeFetchClient} from "../src";

interface MyIndex extends Index {
    resources: TestResource
}

describe('Create client with by index', () => {
    test('Create instance', async () => {
        const client = useNativeFetchClient(HOST);
        const index = await fetchIndexDocument<MyIndex>(client, HOST);
        const api = useJsonApiWithIndex<MyIndex>(index,client);
        expect(api).toBeDefined();
        expect(api.resources).toBeDefined();
        const doc = await api.resources.id('1').self();
        const res = doc!.data as TestResource;
        const value = res.relationships.one.data?.attributes?.prop;
        expect(value).toBe('foo');
        const type = res.relationships.one.data?.type;
        expect(type).toBe('resources');
        expect(res).toBeTruthy();
        expect(res.type).toBe('resources');
    });
});
