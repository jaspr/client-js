import { expect, test, describe } from 'vitest'
import {useNativeFetchClient} from "../src";
import {HOST} from "./Server";

describe('NativeFetchClient test suite', () => {
    test('One Request', () => {
        const cancel = new AbortController();
        const client = useNativeFetchClient();
        const req = client.get(`${HOST}/delay`, cancel.signal);
        cancel.abort();
        return req
            .then(res => expect(res).toBeUndefined())
            .catch(e => expect(e).toBeInstanceOf(DOMException));
    })
    test('Many Request', () => {
        const client = useNativeFetchClient();
        const cancel1 = new AbortController();
        const req1 = client.get(`${HOST}/delay`, cancel1.signal);
        const cancel2 = new AbortController();
        const req2 = client.get(`${HOST}/delay`, cancel2.signal);
        const cancel3 = new AbortController();
        const req3 = client.get(`${HOST}/delay`, cancel3.signal);

        req1.catch(e => expect(e).toBeInstanceOf(DOMException));
        req2.catch(e => expect(e).toBeInstanceOf(DOMException));
        req2.catch(e => expect(e).toBeInstanceOf(DOMException));

        return Promise.allSettled([req1, req2, req3]);
    })
    test('Change header', async () => {
        const client = useNativeFetchClient();
        client.setHeader('Authorization', 'abc123');
        const res1 = await client.get(`${HOST}/headers`);
        const data1 = await res1.json();
        expect(data1.headers.authorization).toBe('abc123')
        client.setHeader('Authorization', '123abc');
        const res2 = await client.get(`${HOST}/headers`);
        const data2 = await res2.json();
        expect(data2.headers.authorization).toBe('123abc');
    })
});
