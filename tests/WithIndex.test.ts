import { expect, test, describe } from 'vitest'
import {HOST, TestResource} from "./Server";
import {fetchIndexDocument, Index, useJsonApiWithIndex, useNativeFetchClient} from "../src";

interface MyIndex extends Index {
    resources: TestResource
}

describe('Create client with by index', () => {
    test('Create instance', async () => {
        const client = useNativeFetchClient(HOST);
        const index = await fetchIndexDocument<MyIndex>(client, HOST);
        const api = useJsonApiWithIndex<MyIndex>(index,client);
        expect(api).toBeDefined();
        expect(api.resources).toBeDefined();
        const doc = await api.resources.self();
        expect(doc!.data[0]).toBeTruthy()
        expect(doc!.data[0].type).toBe('resources');
    });
});
