import { expect, test, describe } from 'vitest'
import {QuatrodotExpressionStrategy} from "../src";


describe('ValueTest', () => {
    test('testConstructor', () => {
        expect(() => QuatrodotExpressionStrategy.literal(undefined)).toThrowError();
    });
    test('testNull', () => {
        expect(QuatrodotExpressionStrategy.literal(null).express()).toBe('null');
    });
    test('testDate', () => {
        expect(QuatrodotExpressionStrategy.literal(new Date('2020-01-01')).express()).toBe('2020-01-01T00:00:00.000Z');
    });
    test('testString', () => {
        expect(QuatrodotExpressionStrategy.literal('string').express()).toBe('string');
    });
    test('testStringEscaped', () => {
        expect(QuatrodotExpressionStrategy.literal('str\'ing').express()).toBe('str\'ing');
    });
    test('testNumber', () => {
        expect(QuatrodotExpressionStrategy.literal(123.123).express()).toBe('123.123');
        expect(QuatrodotExpressionStrategy.literal(0).express()).toBe('0');
        expect(QuatrodotExpressionStrategy.literal(123).express()).toBe('123');
        expect(QuatrodotExpressionStrategy.literal(-1).express()).toBe('-1');
    });
    test('testBoolean', () => {
        expect(QuatrodotExpressionStrategy.literal(true).express()).toBe('true');
        expect(QuatrodotExpressionStrategy.literal(false).express()).toBe('false');
    });
    test('testArray', () => {
        expect(QuatrodotExpressionStrategy.literal([1, 'a', true]).express()).toBe('1::a::true');
    });
});
