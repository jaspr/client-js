import { expect, test, describe } from 'vitest'
import {ODataExpressionStrategy, PrettyExpressionBuilder, QuatrodotExpressionStrategy} from "../src";
import {Expression} from "../src";
import {TestResource} from "./Server";


describe('PrettyExpressionBuilder Test', () => {
    describe('QuatrodotExpressionBuilder as builder', () => {
        test('Equal', () => {
            const builder = PrettyExpressionBuilder<TestResource>(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').eq().literal('bar').build();
            expect(ex?.express()).toBe('foo::eq::bar');
        })
        test('Not Equal', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').ne().literal('bar').build();
            expect(ex?.express()).toBe('foo::ne::bar');
        })
        test('Greater Than', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').gt().literal('bar').build();
            expect(ex?.express()).toBe('foo::gt::bar');
        })
        test('Greater Than Or Equal', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').ge().literal('bar').build();
            expect(ex?.express()).toBe('foo::ge::bar');
        })
        test('Lower Than', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').lt().literal('bar').build();
            expect(ex?.express()).toBe('foo::lt::bar');
        })
        test('Lower Than Or Equal', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').le().literal('bar').build();
            expect(ex?.express()).toBe('foo::le::bar');
        })
        test('In', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').in().literal(['bar', 'bor']).build();
            expect(ex?.express()).toBe('foo::in::bar::bor');
        })
        test('Between', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').be().literal('today', 'tomorrow').build();
            expect(ex?.express()).toBe('foo::be::today::tomorrow');
        })
        test('Starts With', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').startsWith('bar').build();
            expect(ex?.express()).toBe('foo::startsWith::bar');
        })
        test('Ends With', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').endsWith('bar').build();
            expect(ex?.express()).toBe('foo::endsWith::bar');
        })
        test('Contains', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder.field('foo').contains('bar').build();
            expect(ex?.express()).toBe('foo::contains::bar');
        })
        test('test booleans', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            const ex = builder
                .field('p1').eq().literal('v1')
                .and()
                .field('p2').startsWith('v2')
                .or()
                .field('p3').contains('v3')
                .and()
                .field('p4').ne().literal(0)
                .build();
            expect(ex?.express()).toBe('p1::eq::v1|p2::startsWith::v2|p3::contains::v3|p4::ne::0')
        });
        test('Grouping with previous', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            builder.field('fap').lt().literal('tap');
            if (true) {
                builder.and().field('foo').eq().literal('bar');
            }
            if (!false) {
                builder.or().field('fas').ne().literal('tar');
            }
            const ex = builder.build();
            expect(ex?.express()).toBe('fap::lt::tap|foo::eq::bar|fas::ne::tar');
        })
        test('Grouping without previous', () => {
            const builder = PrettyExpressionBuilder(QuatrodotExpressionStrategy);
            if (true) {
                builder.and().field('foo').eq().literal('bar');
            }
            if (!false) {
                builder.or().field('fas').ne().literal('tar');
            }
            const ex = builder.build();
            expect(ex?.express()).toBe('foo::eq::bar|fas::ne::tar');
        })
    })
    describe('ODataExpressionBuilder as builder', () => {
        test('Equal', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').eq().literal('bar').build();
            expect(ex?.express()).toBe('foo eq \'bar\'');
        })
        test('Not Equal', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').ne().literal('bar').build();
            expect(ex?.express()).toBe('foo ne \'bar\'');
        })
        test('Greater Than', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').gt().literal('bar').build();
            expect(ex?.express()).toBe('foo gt \'bar\'');
        })
        test('Greater Than Or Equal', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').ge().literal('bar').build();
            expect(ex?.express()).toBe('foo ge \'bar\'');
        })
        test('Lower Than', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').lt().literal('bar').build();
            expect(ex?.express()).toBe('foo lt \'bar\'');
        })
        test('Lower Than Or Equal', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').le().literal('bar').build();
            expect(ex?.express()).toBe('foo le \'bar\'');
        })
        test('In', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').in().literal(['bar', 'bor']).build();
            expect(ex?.express()).toBe('foo in (\'bar\',\'bor\')');
        })
        test('Between', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').be().literal('today', 'tomorrow').build();
            expect(ex?.express()).toBe('foo be (\'today\',\'tomorrow\')');
        })
        test('Starts With', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').startsWith('bar').build();
            expect(ex?.express()).toBe('startsWith(foo,\'bar\')');
        })
        test('Ends With', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').endsWith('bar').build();
            expect(ex?.express()).toBe('endsWith(foo,\'bar\')');
        })
        test('Contains', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder.field('foo').contains('bar').build();
            expect(ex?.express()).toBe('contains(foo,\'bar\')');
        })
        test('Ands Ors', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            const ex = builder
                .field('p1').eq().literal('v1')
                .and()
                .field('p2').startsWith('v2')
                .or()
                .field('p3').contains('v3')
                .and()
                .field('p4').add(ODataExpressionStrategy.literal(4)).neg().ne().literal(0)
                .build();
            expect(ex?.express()).toBe('p1 eq \'v1\' and startsWith(p2,\'v2\') or contains(p3,\'v3\') and - p4 add 4 ne 0')
        })
        test('Composing with previous', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            builder.field('fap').lt().literal('tap');
            if (true) {
                builder.and().field('foo').eq().literal('bar');
            }
            if (!false) {
                builder.or().field('fas').ne().literal('tar');
            }
            const ex = builder.build();
            expect(ex?.express()).toBe('fap lt \'tap\' and foo eq \'bar\' or fas ne \'tar\'');
        })
        test('Composing without previous', () => {
            const builder = PrettyExpressionBuilder(ODataExpressionStrategy);
            if (true) {
                builder.and().field('foo').eq().literal('bar');
            }
            if (!false) {
                builder.or().field('fas').ne().literal('tar');
            }
            const ex = builder.build();
            expect(ex?.express()).toBe('foo eq \'bar\' or fas ne \'tar\'');
        })
        test('Grouping AND', () => {
            const ex1 = PrettyExpressionBuilder(ODataExpressionStrategy).field('foo').eq().literal('bar').build() as Expression;
            const ex2 = PrettyExpressionBuilder(ODataExpressionStrategy).field('fas').ne().literal('tar').build() as Expression;
            const composite = PrettyExpressionBuilder(ODataExpressionStrategy).conjunct(ex1, ex2);
            const ex = composite.build();
            expect(ex?.express()).toBe('(foo eq \'bar\' and fas ne \'tar\')');
        })
        test('Grouping OR', () => {
            const ex1 = PrettyExpressionBuilder(ODataExpressionStrategy).field('foo').eq().literal('bar').build() as Expression;
            const ex2 = PrettyExpressionBuilder(ODataExpressionStrategy).field('fas').ne().literal('tar').build() as Expression;
            const composite = PrettyExpressionBuilder(ODataExpressionStrategy).disjunct(ex1, ex2);
            const ex = composite.build();
            expect(ex?.express()).toBe('(foo eq \'bar\' or fas ne \'tar\')');
        })
        test('Grouping with following', () => {
            const ex1 = PrettyExpressionBuilder(ODataExpressionStrategy).field('foo').eq().literal('bar').build() as Expression;
            const ex2 = PrettyExpressionBuilder(ODataExpressionStrategy).field('fas').ne().literal('tar').build() as Expression;
            const composite = PrettyExpressionBuilder(ODataExpressionStrategy).disjunct(ex1, ex2);
            const ex = composite.and().field('faz').eq().literal('baz').build();
            expect(ex?.express()).toBe('(foo eq \'bar\' or fas ne \'tar\') and faz eq \'baz\'');
        })
        test('Case with passed filter', () => {
            const selectedIds = [1, 2, 3];
            const passed: Expression = PrettyExpressionBuilder().field('foo').eq().literal('bar').build() as Expression;
            const case1: Expression = PrettyExpressionBuilder().conjunct(
                passed,
                ...selectedIds.map((id) => PrettyExpressionBuilder().field('id').ne().literal(id).build() as Expression
                )).build() as Expression;
            const case2: Expression = PrettyExpressionBuilder().conjunct(
                ...selectedIds.map((id) => PrettyExpressionBuilder().field('id').ne().literal(id).build() as Expression)
            ).build() as Expression;
            expect(case1.express()).toBe('(foo eq \'bar\' and id ne 1 and id ne 2 and id ne 3)');
            expect(case2.express()).toBe('(id ne 1 and id ne 2 and id ne 3)');

        })
        test('Case with passed filter collection', () => {

            const filters = {
                'filter1': {
                    expression: PrettyExpressionBuilder().field('foo').eq().literal('bar').build()
                },
                'filter2': {
                    expression: PrettyExpressionBuilder().field('faz').ne().literal('baz').build()
                },
                'filter3': {
                    expression: null
                },
            };
            const filterExpressions: Expression[] = [];
            if (filters) {
                filterExpressions.push(
                    ...Object.entries(filters)
                        .filter(([, filter]) => Boolean(filter.expression))
                        .map(([, filter]) => filter.expression!)
                );
            }

            const ex = PrettyExpressionBuilder().conjunct(...filterExpressions).build() as Expression;
            expect(ex.express()).toBe('(foo eq \'bar\' and faz ne \'baz\')');
        })
    })
})
