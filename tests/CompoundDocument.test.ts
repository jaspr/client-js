import {expect, test, describe} from 'vitest'
import {
    CollectionDocument,
    ErrorDocument,
    HOST,
    identifier,
    RelationshipCollectionDocument,
    RelatedCollectionDocument,
    RelationshipDocument,
    resource,
    ResourceDocument,
    TestResource
} from "./Server";
import {PartialResource, Resource, useJsonApiWithoutIndex, useNativeFetchClient} from '../src';

const api = useJsonApiWithoutIndex(useNativeFetchClient(HOST));

describe('CompoundDocument test suite', () => {
    test('Fetch All Resources', async () => {
        const doc = await api.select<TestResource>(identifier.type).self();
        expect(doc!.data).toBeInstanceOf(Array);
        expect(doc!.data).toHaveLength(2);
        expect(doc!.self).toBeDefined();
        expect(doc!.first).toBeDefined();
        expect(doc!.last).toBeDefined();
        expect(doc!.next).toBeDefined();
        expect(doc!.prev).toBeDefined();
        expect(doc).toMatchObject(CollectionDocument);
    })
    test('Fetch One Resource', async () => {
        let t = await api.select<TestResource>(identifier.type).id(identifier.id).self();
        expect(t!.self).toBeDefined();
        expect(t!.data!.self).toBeDefined();
        expect(t!.data!.update).toBeDefined();
        expect(t!.data!.delete).toBeDefined();
        expect(t!.data!.param).toBeDefined();
        expect(t!.data!.fields).toBeDefined();
        expect(t!.data!.include).toBeDefined();
        expect(t).toMatchObject(ResourceDocument);

        let p = await t!.data!.self();
        expect(p).toMatchObject(ResourceDocument);

        let d = await api.select<Resource>(identifier.type).id(identifier.id).self();
        expect(d!.data!.self).toBeDefined();
        expect(d!.data!.update).toBeDefined();
        expect(d!.data!.delete).toBeDefined();
        expect(t!.data!.param).toBeDefined();
        expect(t!.data!.fields).toBeDefined();
        expect(t!.data!.include).toBeDefined();
        expect(d).toMatchObject(ResourceDocument);
    })
    test('Create Resource', async () => {
        const r: PartialResource<TestResource> = {
            type: 'resources',
            attributes: {
                prop: 'value'
            }
        }
        const doc = await api.select<TestResource>(identifier.type).create(r);
        expect(doc).toMatchObject(ResourceDocument)
    })
    test('Update Resource', async () => {
        let doc = await api.select<TestResource>(identifier.type).id(identifier.id).self();
        const res = await doc!.data?.update(resource as PartialResource<TestResource>);
        expect(res).toMatchObject(ResourceDocument)
    })
    test('Delete Resource', async () => {
        let doc = await api.select<TestResource>(identifier.type).id(identifier.id).self();
        const res = await doc!.data?.delete();
        expect(res).toBeNull();
    })
    test('Get Single Relationship', async () => {
        const doc = await api.select<TestResource>(identifier.type).id(identifier.id).self();
        expect(doc!.data!.relationships.one.data?.id).toBeDefined();
        expect(doc!.data!.relationships.one).toBeDefined();
        expect(doc!.data!.relationships.one.self).toBeDefined()
        expect(doc!.data!.relationships.one.related).toBeDefined()
        expect(doc!.data!.relationships.one.fields).toBeDefined()
        expect(doc!.data!.relationships.one.param).toBeDefined()
        expect(doc!.data!.relationships.one.include).toBeDefined()
        expect(doc!.data!.relationships.one.update).toBeDefined()
        const rel = await doc!.data!.relationships.one.self();
        expect(rel).toMatchObject(RelationshipDocument);
    })
    test('Get Many Relationship', async () => {
        const doc = await api.select<TestResource>(identifier.type).id(identifier.id).self();
        expect(doc!.data!.relationships.many.data?.shift()?.id).toBeDefined()
        expect(doc!.data!.relationships.many.self).toBeDefined()
        expect(doc!.data!.relationships.many.related).toBeDefined()
        expect(doc!.data!.relationships.many.filter).toBeDefined()
        expect(doc!.data!.relationships.many.fields).toBeDefined()
        expect(doc!.data!.relationships.many.include).toBeDefined()
        expect(doc!.data!.relationships.many.limitOffset).toBeDefined()
        expect(doc!.data!.relationships.many.numberSize).toBeDefined()
        expect(doc!.data!.relationships.many.sort).toBeDefined()
        expect(doc!.data!.relationships.many.param).toBeDefined()
        expect(doc!.data!.relationships.many.update).toBeDefined()
        expect(doc!.data!.relationships.many.delete).toBeDefined()
        expect(doc!.data!.relationships.many.create).toBeDefined()
        // @ts-ignore
        const rel = await doc.data.relationships.many.self();
        expect(rel).toMatchObject(RelationshipCollectionDocument);
        const related = await doc!.data!.relationships.many.related();
        expect(related).toMatchObject(RelatedCollectionDocument);
    })
    test('Not Found Error', async () => {
        try {
            await api.select<TestResource>('foo').self();
        } catch (doc: any) {
            expect(doc.data).toBeUndefined();
            expect(doc.errors).toBeDefined();
            // @ts-ignore
            expect(doc).toMatchObject(ErrorDocument)
        }

    })
    test('Server Error', async () => {
        try {
            await api.select<TestResource>('/error').fields("tests", []).self();
        } catch (doc: any) {
            expect(doc.data).toBeUndefined()
            expect(doc.errors).toBeDefined();
            // @ts-ignore
            expect(doc).toMatchObject(ErrorDocument)
        }
    })
    test('Abort', () => {
        const cancel = new AbortController();
        const req = api.select<TestResource>(identifier.type).self(cancel.signal);
        cancel.abort();
        return req.then(res => expect(res).toBeUndefined()).catch(e => expect(e).toBeInstanceOf(DOMException))
    })
    test('Pagination', async () => {
        try {
            await api.select<TestResource>(identifier.type)
                .limitOffset(1, 1)
                .numberSize(2, 2)
                .self()
        } catch (e) {
            expect(e).instanceof(Error);
            expect((e as Error).message).string('PageSize strategy and LimitOffset strategy cannot be used simultaneously')
        }

    })
});
