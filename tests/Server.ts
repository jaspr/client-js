import createFetchMock, {MockResponseInitFunction} from 'vitest-fetch-mock';
import {vi} from 'vitest';
import Client from "../src/Client";
import {Resource, ResourceIdentifier, ResourceObject, ToManyRelationship, ToOneRelationship} from "../src";

const fetchMock = createFetchMock(vi);
fetchMock.enableMocks();

const HOST = 'https://test.com/';
const meta = {key: 'value'};
const jsonapi = {version: '1.0'}
const identifier: ResourceIdentifier = {
    type: 'resources',
    id: '1'
}

export interface TestResource extends Resource {
    type: 'resources',
    attributes: {
        prop: string
    },
    relationships: {
        one: ToOneRelationship<TestResource>
        many: ToManyRelationship<TestResource>
    }
}

const identifier1: ResourceIdentifier = {
    type: 'resources',
    id: '2'
}
const resource1: ResourceObject ={
    ...identifier1,
    attributes:{
        prop: 'foo'
    },
    relationships: {
        recursion: {
            data: identifier,
            links: {
                self: `${HOST}/resources/2/relationships/recursion`,
                related: `${HOST}/resources/2/recursion`
            }
        }
    }
}
const identifier2: ResourceIdentifier = {
    type: 'resources',
    id: '3'
}
const resource2: ResourceObject ={
    ...identifier2,
    attributes:{
        prop: 'bar'
    }
}
const resource: ResourceObject = {
    ...identifier,
    attributes: {
        prop: 'value'
    },
    relationships: {
        one: {
            data: identifier1,
            links: {
                self: `${HOST}/resources/1/relationships/one`,
                related: `${HOST}/resources/1/one`
            }
        },
        many: {
            data: [
                identifier1,
                identifier2
            ],
            links: {
                self: `${HOST}/resources/1/relationships/many`,
                related: `${HOST}/resources/1/many`
            }
        }

    },
    links: {
        self: `${identifier.type}/${identifier.id}`
    },
    meta: {
        foo: 'bar'
    }
};
const ResourceDocument = {
    jsonapi,
    meta,
    data: resource,
    included: [resource1, resource2],
    links: {
        self: `${HOST}/resources/1`
    }
};
const CollectionDocument = {
    jsonapi,
    meta,
    data: [resource, resource],
    included: [resource1, resource2],
    links: {
        self: `${HOST}/resources`,
        first: `${HOST}/resources?page[offset]=0`,
        next: `${HOST}/resources?page[offset]=10&page[limit]=10`,
        prev: `${HOST}/resources?page[offset]=0&page[limit]=10`,
        last: `${HOST}/resources?page[offset]=100&page[limit]=10`,
    }
};
const RelationshipDocument = {
    jsonapi,
    meta,
    data: identifier1,
    links: {
        self: `${HOST}/resources/1/relationships/one`,
        related: `${HOST}/resources/1/one`
    }
};
const RelationshipCollectionDocument = {
    jsonapi,
    meta,
    data: [identifier1,identifier2],
    links: {
        self: `${HOST}/resources/1/relationships/many`,
        related: `${HOST}/resources/1/many`,
        first: `${HOST}/resources/1/relationships/many?page[offset]=0`,
        next: `${HOST}/resources/1/relationships/many?page[offset]=10&page[limit]=10`,
        prev: `${HOST}/resources/1/relationships/many?page[offset]=0&page[limit]=10`,
        last: `${HOST}/resources/1/relationships/many?page[offset]=100&page[limit]=10`,
    }
};
const RelatedCollectionDocument = {
    jsonapi,
    meta,
    data: [resource1, resource2],
    links: {
        self: `${HOST}/resources/1/many`,
        related: `${HOST}/resources/1/many`,
        first: `${HOST}/resources/1/many?page[offset]=0`,
        next: `${HOST}/resources/1/many?page[offset]=10&page[limit]=10`,
        prev: `${HOST}/resources/1/many?page[offset]=0&page[limit]=10`,
        last: `${HOST}/resources/1/many?page[offset]=100&page[limit]=10`,
    }
}
const ErrorDocument = {
    jsonapi,
    errors: [
        {
            id: 'error-id',
            links: {
                about: `${HOST}/links/errors/error-id`
            },
            status: '500',
            code: '1234',
            title: 'Error Title',
            detail: 'Detail information about error',
            source: {
                pointer: '/data',
                parameter: 'filter'
            },
            meta
        }
    ]
}

const response: MockResponseInitFunction = async (request) => {
    if (request.url.endsWith('/delay')) {
        return new Promise((resolve, reject) => {
            setTimeout(() => resolve({status: 200}), 500);
        })
    }
    if (request.url.endsWith('/headers')) {
        const hs: { [key: string]: string } = {};
        request.headers.forEach((value, key, parent) => {
            hs[key] = value
        })
        return {
            status: 200,
            body: JSON.stringify({
                headers: hs
            })
        }
    }
    if (!request.headers.has('Accept')
        || request.headers.get('Accept') !== 'application/vnd.api+json') {
        throw ('Headers are incorrect');
    }
    if (!request.headers.has('Content-Type')
        || request.headers.get('Content-Type') !== 'application/vnd.api+json') {
        throw ('Headers are incorrect');
    }

    if (request.url.endsWith('/resources')) {
        if (request.method === 'POST') {
            return {
                status: 201,
                body: JSON.stringify(ResourceDocument)
            }
        } else {
            return {
                status: 200,
                body: JSON.stringify(CollectionDocument)
            }
        }
    } else if (request.url.endsWith('/resources/1')) {
        if (request.method === 'PATCH') {
            return {
                status: 200,
                body: JSON.stringify(ResourceDocument)
            }
        } else if (request.method === 'GET') {
            return {
                status: 200,
                body: JSON.stringify(ResourceDocument)
            }
        } else if (request.method === 'DELETE') {
            return {
                status: 204
            }
        } else {
            return {
                status: 405,
                body: JSON.stringify(ErrorDocument)
            }
        }
    } else if (request.url.endsWith('/resources/1/relationships/one')) {
        if (request.method === 'PATH') {
            return {
                status: 200,
                body: JSON.stringify(RelationshipDocument)
            }
        } else {
            return {
                status: 200,
                body: JSON.stringify(RelationshipDocument)
            }
        }
    } else if (request.url.endsWith('/resources/1/relationships/many')) {
        if (request.method === 'PATH') {
            return {
                status: 200,
                body: JSON.stringify(RelationshipCollectionDocument)
            }
        } else if (request.method === 'POST') {
            return {
                status: 200,
                body: JSON.stringify(RelationshipCollectionDocument)
            }
        } else if (request.method === 'DELETE') {
            return {
                status: 200,
                body: JSON.stringify(RelationshipCollectionDocument)
            }
        } else {
            return {
                status: 200,
                body: JSON.stringify(RelationshipCollectionDocument)
            }
        }
    } else if (request.url.endsWith('/resources/1/many')) {
        if (request.method === 'PATH') {
            return {
                status: 200,
                body: JSON.stringify(RelatedCollectionDocument)
            }
        } else if (request.method === 'POST') {
            return {
                status: 200,
                body: JSON.stringify(RelatedCollectionDocument)
            }
        } else if (request.method === 'DELETE') {
            return {
                status: 200,
                body: JSON.stringify(RelatedCollectionDocument)
            }
        } else {
            return {
                status: 200,
                body: JSON.stringify(RelatedCollectionDocument)
            }
        }
    } else if (request.url.endsWith('/foo')) {
        return {
            status: 404,
            body: JSON.stringify(ErrorDocument)
        }
    } else if (request.url.endsWith('/')) {
        return {
            status: 200,
            body: JSON.stringify({
                "jsonapi": {
                    "version": "1.0"
                },
                "links": {
                    "resources": `${HOST}/${identifier.type}`
                },
                "meta": {
                    "title": "JSON:API Index Page",
                    "baseUrl": `${HOST}`
                }
            })

        }
    } else {
        return {
            status: 500,
            body: JSON.stringify(ErrorDocument)
        }
    }
};
fetchMock.mockIf(/^https?:\/\/test.com.*$/, response)

const DummyClient: Client = {

    delete(url: string): Promise<Response> {
        return new Promise(() => {
            console.log(url);
            return {};
        });
    },

    get(url: string): Promise<Response> {
        return new Promise(() => {
            console.log(url);
            return {};
        });
    },

    patch(url: string, body: BodyInit): Promise<Response> {
        return new Promise(() => {
            console.log(url, body);
            return {};
        });
    },

    post(url: string, body: BodyInit): Promise<Response> {
        return new Promise(() => {
            console.log(url, body);
            return {};
        });
    },

    setHeader(name: string, value: string): void {
        console.log(name, value);
    },

}

export {
    HOST,
    DummyClient,
    jsonapi,
    meta,
    identifier,
    resource,
    ResourceDocument,
    CollectionDocument,
    RelationshipDocument,
    RelationshipCollectionDocument,
    RelatedCollectionDocument,
    ErrorDocument
};
