import { expect, test, describe } from 'vitest'
import {ODataExpressionStrategy} from "../src";


describe('ValueTest', () => {
    test('testConstructor', () => {
        expect(() => ODataExpressionStrategy.literal(undefined)).toThrowError();
    });
    test('testNull', () => {
        expect(ODataExpressionStrategy.literal(null).express()).toBe('null');
    });
    test('testDate', () => {
        expect(ODataExpressionStrategy.literal(new Date('2020-01-01')).express()).toBe('datetime\'2020-01-01T00:00:00.000Z\'');
    });
    test('testString', () => {
        expect(ODataExpressionStrategy.literal('string').express()).toBe('\'string\'');
    });
    test('testStringEscaped', () => {
        expect(ODataExpressionStrategy.literal('str\'ing').express()).toBe('\'str\'\'ing\'');
    });
    test('testNumber', () => {
        expect(ODataExpressionStrategy.literal(123.123).express()).toBe('123.123');
        expect(ODataExpressionStrategy.literal(0).express()).toBe('0');
        expect(ODataExpressionStrategy.literal(123).express()).toBe('123');
        expect(ODataExpressionStrategy.literal(-1).express()).toBe('-1');
    });
    test('testBoolean', () => {
        expect(ODataExpressionStrategy.literal(true).express()).toBe('true');
        expect(ODataExpressionStrategy.literal(false).express()).toBe('false');
    });
    test('testArray', () => {
        expect(ODataExpressionStrategy.literal([1,'a',true]).express()).toBe('(1,\'a\',true)');
    });
});
