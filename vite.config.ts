/// <reference types="vitest" />
import {resolve} from 'path'
import {type ConfigEnv, defineConfig} from 'vite';
import dts from 'vite-plugin-dts';

export default ({mode}: ConfigEnv) => {
    return defineConfig({
        build: {
            lib: {
                entry: resolve(__dirname, 'src/index.ts'),
                name: 'jaspr',
                formats: ['es','umd','iife'],
                // fileName: (format) => `client.${format}.js`,
            },
            sourcemap: true
        },
        plugins: [dts({
            insertTypesEntry:true,
            outDir:'dist/types',
            strictOutput:false,
        })],
        test:{
            environment: 'jsdom',
            setupFiles: [
                resolve(__dirname, 'tests/Server.ts'),
            ],
            dir: resolve(__dirname, 'tests'),
        }
    })
}
