export default interface Client {

    get(url: string, signal?: AbortSignal): Promise<Response>;

    post(url: string, body: BodyInit, signal?: AbortSignal): Promise<Response>;

    patch(url: string, body: BodyInit, signal?: AbortSignal): Promise<Response>;

    delete(url: string, body?: BodyInit, signal?: AbortSignal): Promise<Response>;

    setHeader(name: string, value: string): void;
}
