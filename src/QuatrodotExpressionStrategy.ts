import {
    BinaryExpression,
    Constants,
    Expression,
    ExpressionStrategy,
    ExpressionGroup,
    FunctionExpression,
    GroupExpression
} from "./Expression";

const unsupported: Constants[] = [
    Constants.FUNCTION_CONCAT,
    Constants.FUNCTION_INDEX_OF,
    Constants.FUNCTION_LENGTH,
    Constants.FUNCTION_SUBSTRING,
    Constants.ARITHMETIC_ADDITION,
    Constants.ARITHMETIC_SUBTRACTION,
    Constants.ARITHMETIC_NEGATION,
    Constants.ARITHMETIC_MULTIPLICATION,
    Constants.ARITHMETIC_DIVISION,
    Constants.ARITHMETIC_MODULO,
    Constants.FUNCTION_MATCHES_PATTERN,
    Constants.FUNCTION_TO_LOWER,
    Constants.FUNCTION_TO_UPPER,
    Constants.FUNCTION_TRIM,
    Constants.FUNCTION_DATE,
    Constants.FUNCTION_DAY,
    Constants.FUNCTION_HOUR,
    Constants.FUNCTION_MINUTE,
    Constants.FUNCTION_MONTH,
    Constants.FUNCTION_NOW,
    Constants.FUNCTION_SECOND,
    Constants.FUNCTION_TIME,
    Constants.FUNCTION_YEAR,
    Constants.FUNCTION_CEILING,
    Constants.FUNCTION_FLOOR,
    Constants.FUNCTION_ROUND,
]
const check = (constant: Constants): void => {
    if(unsupported.includes(constant)){
        throw new Error(`Unsupported operator ${constant}.`);
    }
}
const field = function (name: string): Expression {
    return {
        express(): string {
            return name;
        }
    }
}
const literal = function (value: any): Expression {
    let _value: string;
    if (value === undefined) {
        throw new Error('Expected value to by one of [null, Date, String, Number, Boolean, Array], but got ' + typeof value);
    }
    if (value === null) {
        _value = encodeURIComponent(value)
    } else {
        if (typeof value.express === 'function') {
            _value = value.express();
        } else if (value instanceof Date) {
            _value = `${value.toISOString()}`;
        } else if (value instanceof Array) {
            value = value.map(item => literal(item).express());
            _value = `${value.join('::')}`;
        } else if (typeof value === 'string') {
            _value = encodeURIComponent(value)
        } else if (typeof value === 'boolean') {
            _value = encodeURIComponent(value)
        } else if (typeof value === 'number') {
            _value = encodeURIComponent(value)
        } else {
            throw new Error('Expected value to by one of [null, Date, String, Number, Boolean, Array], but got ' + typeof value);
        }
    }
    return {
        express: function (): string {
            return _value;
        }
    };
}
const groupExpression: GroupExpression = function (expressions: Expression[] = []): ExpressionGroup {
    let _expressions = expressions;
    return {
        addExpression(expression: Expression): ExpressionGroup {
            _expressions.push(expression);
            return this;
        },
        express(): string {
            return `${_expressions.map(item => item.express()).join('|')}`;
        }
    }
}
const binaryExpression: BinaryExpression = function (left: Expression, operator: Constants, right: Expression): Expression {
    check(operator)
    return {
        express(): string {
            if (([Constants.LOGICAL_AND, Constants.LOGICAL_OR] as Constants[]).includes(operator)) {
                return `${left.express()}|${right.express()}`;
            }
            return `${left.express()}::${operator}::${right.express()}`;
        }
    }
}
const functionExpression: FunctionExpression = function (fn: Constants, args: Expression[]): Expression {
    check(fn);
    const left = args[0];
    const right = args[1];
    return {
        express(): string {
            return `${left.express()}::${fn}::${right.express()}`
        }
    };
}
const unaryExpression = function (value: Expression, operator: Constants): Expression {
    throw new Error("Unsupported expression.")
}
const strategy: ExpressionStrategy = {
    field,
    literal,
    unaryExpression,
    groupExpression,
    binaryExpression,
    functionExpression,
}
export default strategy;
