import ODataExpressionStrategy from "./ODataExpressionStrategy";
import {Constants, Expression, ExpressionStrategy} from "./Expression";
import {ResourceObject} from "./index";

interface Operator<T extends ResourceObject> {
    eq(): Value<T>;

    ne(): Value<T>;

    gt(): Value<T>;

    ge(): Value<T>;

    lt(): Value<T>;

    le(): Value<T>;

    be(): Range<T>;

    in(): Value<T>;

    has(): Value<T>;

    startsWith(text: string): Compose<T>;

    endsWith(text: string): Compose<T>;

    contains(text: string): Compose<T>;

    concat(fieldOrValue: Expression): Operator<T>;

    indexOf(text: string): Operator<T>;

    add(fieldOrValue: Expression): Operator<T>;

    sub(fieldOrValue: Expression): Operator<T>;

    mul(fieldOrValue: Expression): Operator<T>;

    div(fieldOrValue: Expression): Operator<T>;

    mod(fieldOrValue: Expression): Operator<T>;

    not(field: Expression): Operator<T>;

    matchesPattern(pattern: string): Compose<T>;

    substring(length: number): Operator<T>;

    toLower(): Operator<T>;

    toUpper(): Operator<T>;

    lengthOf(): Operator<T>;

    trim(): Operator<T>;

    floor(): Operator<T>;

    ceiling(): Operator<T>;

    round(): Operator<T>;

    date(): Operator<T>;

    day(): Operator<T>;

    hour(): Operator<T>;

    minute(): Operator<T>;

    month(): Operator<T>;

    second(): Operator<T>;

    time(): Operator<T>;

    year(): Operator<T>;

    neg(): Operator<T>;

}

interface Value<T extends ResourceObject> {
    literal(value: any): Compose<T>;

    /**
     * @deprecated use disjunct()/conjunct() instead
     */
    right(expression: Expression): Compose<T>;
}

interface Range<T extends ResourceObject> {
    literal(from: string | number | Date, to: string | number | Date): Compose<T>;
}

interface Where<T extends ResourceObject> {

    field(name: keyof T["attributes"] | keyof T['relationships'] | string): Operator<T>;
}

interface Compose<T extends ResourceObject> {
    and(): Where<T>;

    or(): Where<T>;

    build(): Expression | null;
}

interface Grouping<T extends ResourceObject> {
    conjunct(...expressions: Expression[]): Compose<T> & Grouping<T>;

    disjunct(...expressions: Expression[]): Compose<T> & Grouping<T>;
}

export interface ExpressionBuilder<T extends ResourceObject> extends Where<T>, Compose<T>, Grouping<T> {
}

export default function <T extends ResourceObject>(strategy?: ExpressionStrategy): ExpressionBuilder<T> {
    const _builder: ExpressionStrategy = strategy ?? ODataExpressionStrategy;
    let _ex: Expression | null = null;
    let _previous: Expression | null = null;
    let _glue: Constants;
    let _left: Expression;
    let _op: Constants;
    let _right: Expression;
    const compose: Compose<T> = {
        and(): Where<T> {
            _previous = this.build();
            _glue = Constants.LOGICAL_AND;
            _ex = null;
            return where;
        },
        or(): Where<T> {
            _previous = this.build();
            _glue = Constants.LOGICAL_OR;
            _ex = null;
            return where;
        },
        build(): Expression | null {
            if (!_ex) {
                if (_left && _op && _right) {
                    _ex = _builder.binaryExpression(_left, _op, _right);
                } else {
                    return null;
                }
            }
            if (_previous) {
                _ex = _builder.binaryExpression(_previous, _glue, _ex)
            }
            return _ex;
        }
    }
    const value: Value<T> = {
        right(expression: Expression): Compose<T> {
            _right = expression;
            return compose;
        },
        literal(value: any): Compose<T> {
            _right = _builder.literal(value);
            return compose;
        }

    }
    const range: Range<T> = {
        literal(from: any, to: any): Compose<T> {
            _right = _builder.literal([from, to]);
            return compose;
        }
    }
    const operator: Operator<T> = {
        add(fieldOrValue: Expression): Operator<T> {
            _left = _builder.binaryExpression(_left, Constants.ARITHMETIC_ADDITION, fieldOrValue);
            return this;
        },
        ceiling(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_CEILING, [_left]);
            return this;
        },
        concat(fieldOrValue: Expression): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_CONCAT, [_left, fieldOrValue]);
            return this;
        },
        date(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_DATE, [_left]);
            return this;
        },
        day(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_DAY, [_left]);
            return this;
        },
        div(fieldOrValue: Expression): Operator<T> {
            _left = _builder.binaryExpression(_left, Constants.ARITHMETIC_DIVISION, fieldOrValue);
            return this;
        },
        floor(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_FLOOR, [_left]);
            return this;
        },
        has(): Value<T> {
            _op = Constants.LOGICAL_HAS;
            return value;
        },
        hour(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_HOUR, [_left]);
            return this;
        },
        indexOf(text: string): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_INDEX_OF, [_left, _builder.literal(text)]);
            return this;
        },
        lengthOf(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_LENGTH, [_left]);
            return this;
        },
        matchesPattern(pattern: string): Compose<T> {
            _ex = _builder.functionExpression(Constants.FUNCTION_MATCHES_PATTERN, [_left, _builder.literal(pattern)]);
            return compose;
        },
        minute(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_MINUTE, [_left]);
            return this;
        },
        mod(fieldOrValue: Expression): Operator<T> {
            _left = _builder.binaryExpression(_left, Constants.ARITHMETIC_MODULO, fieldOrValue);
            return this;
        },
        month(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_MONTH, [_left]);
            return this;
        },
        mul(fieldOrValue: Expression): Operator<T> {
            _left = _builder.binaryExpression(_left, Constants.ARITHMETIC_MULTIPLICATION, fieldOrValue);
            return this;
        },
        neg(): Operator<T> {
            _left = _builder.unaryExpression(_left, Constants.ARITHMETIC_NEGATION);
            return this;
        },
        not(): Operator<T> {
            _left = _builder.unaryExpression(_left, Constants.LOGICAL_NOT);
            return this;
        },
        round(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_ROUND, [_left]);
            return this;
        },
        second(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_SECOND, [_left]);
            return this;
        },
        sub(fieldOrValue: Expression): Operator<T> {
            _left = _builder.binaryExpression(_left, Constants.ARITHMETIC_SUBTRACTION, fieldOrValue);
            return this;
        },
        substring(length: number): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_SUBSTRING, [_left, _builder.literal(length)]);
            return this;
        },
        time(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_SECOND, [_left]);
            return this;
        },
        toLower(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_TO_LOWER, [_left]);
            return this;
        },
        toUpper(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_TO_UPPER, [_left]);
            return this;
        },
        trim(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_TRIM, [_left]);
            return this;
        },
        year(): Operator<T> {
            _left = _builder.functionExpression(Constants.FUNCTION_SECOND, [_left]);
            return this;
        },
        be(): Range<T> {
            _op = Constants.LOGICAL_BETWEEN;
            return range;
        },
        contains(text: string): Compose<T> {
            _ex = _builder.functionExpression(Constants.FUNCTION_CONTAINS, [_left, _builder.literal(text)]);
            return compose;
        },
        endsWith(text: string): Compose<T> {
            _ex = _builder.functionExpression(Constants.FUNCTION_ENDS_WITH, [_left, _builder.literal(text)]);
            return compose;
        },
        eq(): Value<T> {
            _op = Constants.LOGICAL_EQUAL;
            return value;
        },
        ge(): Value<T> {
            _op = Constants.LOGICAL_GREATER_THAN_OR_EQUAL;
            return value;
        },
        gt(): Value<T> {
            _op = Constants.LOGICAL_GREATER_THAN;
            return value;
        },
        in(): Value<T> {
            _op = Constants.LOGICAL_IN;
            return value;
        },
        le(): Value<T> {
            _op = Constants.LOGICAL_LOWER_THAN_OR_EQUAL;
            return value;
        },
        lt(): Value<T> {
            _op = Constants.LOGICAL_LOWER_THAN;
            return value;
        },
        ne(): Value<T> {
            _op = Constants.LOGICAL_NOT_EQUAL;
            return value;
        },
        startsWith(text: string): Compose<T> {
            _ex = _builder.functionExpression(Constants.FUNCTION_STARTS_WITH, [_left, _builder.literal(text)]);
            return compose;
        }

    }
    const where: Where<T> = {
        field(name: keyof T["attributes"] | keyof T['relationships'] | string): Operator<T> {
            _left = _builder.field(name as string);
            return operator;
        }
    }
    const grouping: Grouping<T> = {
        conjunct(...expressions: Expression[]): Compose<T> & Grouping<T> {
            _ex = _builder.groupExpression(expressions, Constants.LOGICAL_AND);
            return {...compose, ...grouping};
        },
        disjunct(...expressions: Expression[]): Compose<T> & Grouping<T> {
            _ex = _builder.groupExpression(expressions, Constants.LOGICAL_OR);
            return {...compose, ...grouping};
        }

    }
    return {...where, ...compose, ...grouping};
}
