import {
    BinaryExpression,
    Constants,
    Expression,
    ExpressionStrategy,
    ExpressionGroup,
    FunctionExpression,
    GroupExpression,
    UnaryExpression
} from "./Expression";

const field = function (name: string): Expression {
    return {
        express(): string {
            return name;
        }
    }
}
const literal = function (value: any): Expression {
    let _value: string;
    if (value === undefined) {
        throw new Error('Expected value to by one of [null, Date, String, Number, Boolean, Array], but got ' + typeof value);
    }

    if (value === null) {
        _value = encodeURIComponent(value);
    } else {
        if (typeof value.express === 'function') {
            _value = value.express();
        } else if (value instanceof Date) {
            _value = `datetime'${value.toISOString()}'`;
        } else if (value instanceof Array) {
            value = value.map(item => literal(item).express());
            _value = `(${value.join(',')})`;
        } else if (typeof value === 'string') {
            _value = `'${encodeURIComponent(value.replace(`'`, `''`))}'`;
        } else if (typeof value === 'boolean') {
            _value = encodeURIComponent(value);
        } else if (typeof value === 'number') {
            _value = encodeURIComponent(value)
        } else {
            throw new Error('Expected value to by one of [null, Date, String, Number, Boolean, Array], but got ' + typeof value);
        }
    }

    return {
        express(): string {
            return _value;
        }
    }
}
const groupExpression: GroupExpression = function (expressions: Expression[], operator: Constants): ExpressionGroup {
    if (operator !== Constants.LOGICAL_AND && operator !== Constants.LOGICAL_OR) {
        throw Error("Invalid group operator. Possible values are: [AND, OR].")
    }
    return {
        addExpression(expression: Expression): ExpressionGroup {
            expressions.push(expression);
            return this;
        },

        express(): string {
            return '(' + expressions.map(item => item.express()).join(` ${operator} `) + ')';
        }
    }
}
const binaryExpression: BinaryExpression = function (left: Expression, operator: Constants, right: Expression): Expression {
    return {
        express(): string {
            return `${left.express()} ${operator} ${right.express()}`;
        }
    }
}
const functionExpression: FunctionExpression = function (fn: Constants, args: Expression[]): Expression {
    return {
        express(): string {
            let res = `${fn}(`;
            if (args.length > 0) {
                let comma = false;
                for (const arg of args) {
                    if (comma) {
                        res += ',';
                    }
                    res += `${arg.express()}`;
                    comma = true;
                }
            }
            res += `)`;
            return res;
        }
    }
}
const unaryExpression: UnaryExpression = function (value: Expression, operator: Constants): Expression {
    return {
        express(): string {
            return `${operator} ${value.express()}`;
        }
    }
}

const strategy: ExpressionStrategy = {
    literal,
    field,
    groupExpression,
    binaryExpression,
    functionExpression,
    unaryExpression
}

export default strategy;
