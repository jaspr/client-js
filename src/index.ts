import type {
    BinaryExpression,
    Expression,
    ExpressionStrategy,
    FieldExpression,
    FunctionExpression,
    GroupExpression,
    LiteralExpression,
    UnaryExpression
} from "./Expression";
import type {
    CompoundDocument,
    Relationship,
    ResourceObject,
    ResourceIdentifier,
    PrimaryData,
    Meta,
    Link
} from "./JSONAPI";
import useNativeFetchClient from "./NativeFetchClient";
import ODataExpressionStrategy from "./ODataExpressionStrategy";
import PrettyExpressionBuilder, {type ExpressionBuilder} from "./PrettyExpressionBuilder";
import QuatrodotExpressionStrategy from "./QuatrodotExpressionStrategy";
import type Client from "./Client";

enum HttpStatusCodes {
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202,
    NO_CONTENT = 204,
    BAD_REQUEST = 400,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    CONFLICT = 409,
    SERVER_ERROR = 500

}

declare type RecursivePartial<T> = { [P in keyof T]?: RecursivePartial<T[P]> | T[P] };

declare type PartialResource<T extends Resource> = RecursivePartial<T>;

declare type ResourceList<T extends Index> = {
    [P in keyof T]: CollectionMethods<T[P]>
}

declare type IndexDocument<T extends Index> = CompoundDocument & {
    links: {
        [P in keyof T]: Link | string
    }
}

declare type SuggestRelationship<T extends Resource> = keyof T['relationships'] | string;
declare type SuggestAttribute<T extends Resource> = keyof T['attributes'] | string;
declare type SuggestedField<T extends Resource> = SuggestAttribute<T> | SuggestRelationship<T> | string;
declare type SuggestedType<T extends Resource> = T['type'] | string;

interface Filterable<T extends Resource> {
    /**
     * Adds custom parameter to query part of URL in format ?name=value
     * @param name
     * @param value
     */
    param(name: string, value: string): this;

    /**
     * Sparse fields from resource type
     * @link https://jsonapi.org/format/1.0/#fetching-sparse-fieldsets
     * @param type
     * @param fields
     */
    fields(type: SuggestedType<T>, fields: SuggestedField<T>[]): this;

    /**
     * Inclusion as defined by JSON Specification v1.0
     * @link https://jsonapi.org/format/1.0/#fetching-includes
     * @param path
     */
    include(path: SuggestRelationship<T>): this;

    /**
     * Sort by field name
     * @link https://jsonapi.org/format/1.0/#fetching-sorting
     * @param field
     * @param desc
     */
    sort(field: SuggestAttribute<T>, desc: boolean): this;

    /**
     * Offset-based strategy
     * @link https://jsonapi.org/format/1.0/#fetching-pagination
     * @param limit
     * @param offset
     */
    limitOffset(limit: number | null, offset: number | null): this;

    /**
     * Page-based strategy
     * @link https://jsonapi.org/format/1.0/#fetching-pagination
     * @param number
     * @param size
     */
    numberSize(number: number | null, size: number | null): this;

    /**
     * Filter by expression from selected strategy
     * @see ODataExpressionStrategy.ts
     * @see QuatrodotExpressionStrategy.ts
     * @param expression
     */
    filter(expression: Expression | string | null): this;
}

interface Traversable<T> {

    next(signal?: AbortSignal): Promise<T | undefined>;

    prev(signal?: AbortSignal): Promise<T | undefined>;

    first(signal?: AbortSignal): Promise<T | undefined>;

    last(signal?: AbortSignal): Promise<T | undefined>
}

interface Identifier extends ResourceIdentifier {
    [unknown: string]: any
}

interface Resource extends ResourceObject {

    relationships?: { [key: string]: ToOneRelationship | ToManyRelationship }

    fields(type: SuggestedType<this>, fields: SuggestedField<this>[]): this;

    include(pointer: SuggestRelationship<this>): this;

    param(name: string, value: string): this;

    self(signal?: AbortSignal): Promise<ResourceDocument<this> | undefined>;

    update(data: PartialResource<this>, signal?: AbortSignal): Promise<ResourceDocument<this> | undefined>;

    delete(signal?: AbortSignal): Promise<ResourceDocument<this> | undefined>;
}

interface ToOneRelationship<T extends Resource = Resource> extends Relationship {

    data?: (Partial<T> & ResourceIdentifier) | null;

    self(signal?: AbortSignal): Promise<IdentifierDocument | undefined>;

    update(identifier: ResourceIdentifier, signal?: AbortSignal): Promise<IdentifierDocument | undefined | null>;

    related(signal?: AbortSignal): Promise<ResourceDocument<T> | undefined>;

    fields(type: SuggestedType<T>, fields: SuggestedField<T>[]): ToOneRelationship<T>;

    include(pointer: SuggestRelationship<T>): ToOneRelationship<T>;

    param(name: string, value: string): ToOneRelationship<T>;

}

interface ToManyRelationship<T extends Resource = Resource> extends Filterable<T>, Relationship {

    data?: (Partial<T> & ResourceIdentifier)[];

    self(signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined>;

    related(signal?: AbortSignal): Promise<ResourceCollectionDocument<T> | undefined>;

    delete(identifiers: ResourceIdentifier[], signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined | null>;

    update(identifiers: ResourceIdentifier[], signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined | null>;

    create(identifiers: ResourceIdentifier[], signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined | null>;
}

interface IdentifierDocument<T extends Resource = Resource> extends CompoundDocument {

    data?: ResourceIdentifier | null;

    self(signal?: AbortSignal): Promise<IdentifierDocument | undefined>;

    related(signal?: AbortSignal): Promise<ResourceDocument<T> | undefined>;
}

interface IdentifierCollectionDocument<T extends Resource = Resource> extends CompoundDocument, Traversable<IdentifierCollectionDocument> {
    data: ResourceIdentifier[];

    self(signal?: AbortSignal): Promise<IdentifierCollectionDocument | undefined>;

    related(signal?: AbortSignal): Promise<ResourceCollectionDocument<T> | undefined>;
}

interface ResourceDocument<T extends Resource> extends CompoundDocument {

    data: T | null;

    self(signal?: AbortSignal): Promise<ResourceDocument<T> | undefined>;
}

interface ResourceCollectionDocument<T extends Resource> extends CompoundDocument, Traversable<ResourceCollectionDocument<T>> {
    data: T[];

    self(signal?: AbortSignal): Promise<ResourceCollectionDocument<T> | undefined>;
}

interface SelectCollection {
    select<T extends Resource>(collection: string | URL): CollectionMethods<T>;
}

interface CollectionMethods<T extends Resource> extends Filterable<T> {
    self(signal?: AbortSignal): Promise<ResourceCollectionDocument<T> | undefined>;

    id(id: string): ResourceMethods<T>;

    create(data: PartialResource<T>, signal?: AbortSignal): Promise<ResourceDocument<T> | undefined | null>
}

interface ResourceMethods<T extends Resource> {
    self(signal?: AbortSignal): Promise<ResourceDocument<T> | undefined>;
}

interface Index {
    [key: string]: Resource
}

interface Criteria<T extends Resource> extends Filterable<T> {
    toURL(): string;
}

const bodyFactory = (data: ResourceIdentifier | ResourceIdentifier[] | PartialResource<Resource> | null): BodyInit => {
    return JSON.stringify({
        jsonapi: {version: '1.0'},
        data
    });
}

const criteriaFactory = <T extends Resource>(): Criteria<T> => {
    let _fields: { [key: string]: string[] } = {};
    let _limit: number | null = null;
    let _offset: number | null = null;
    let _number: number | null = null;
    let _size: number | null = null;
    let _inclusion: string[] = [];
    let _expression: Expression | string | null = null;
    let _sortBy: { [key: string]: boolean } = {};
    let _params: { [key: string]: string } = {};
    return {
        param(name: string, value: string) {
            _params[name] = value;
            return this;
        },
        fields(type: string, fields: string[]) {
            _fields[type] = fields.concat(_fields[type] || []);
            return this;
        },
        include(path: string) {
            _inclusion.push(path);
            return this;
        },
        sort(field: string, desc = false) {
            _sortBy[field] = desc;
            return this;
        },
        limitOffset(limit: number | null, offset: number | null) {
            _limit = limit;
            _offset = offset;
            return this;
        },
        numberSize(number: number | null, size: number | null) {
            _number = number;
            _size = size;
            return this;
        },
        filter(expression: Expression | string | null) {
            _expression = expression;
            return this;
        },
        toURL(): string {
            let query = '';
            let glue = '?';
            if ((_number !== null || _size !== null) && (_limit !== null || _offset !== null)) {
                throw new Error('PageSize strategy and LimitOffset strategy cannot be used simultaneously.')
            }
            if (_limit !== null) {
                query += glue + 'page[limit]=' + _limit.toString();
                glue = '&';
            }
            if (_offset !== null) {
                query += glue + 'page[offset]=' + _offset.toString();
                glue = '&';
            }
            if (_number !== null) {
                query += glue + 'page[number]=' + _number.toString();
                glue = '&';
            }
            if (_size !== null) {
                query += glue + 'page[size]=' + _size.toString();
                glue = '&';
            }
            for (const field in _fields) {
                query += glue + `fields[${field}]=` + _fields[field].join();
                glue = '&';
            }
            if (_expression) {
                query += glue + 'filter=' + (typeof _expression === 'string' ? _expression : _expression.express());
                glue = '&';
            }
            if (_inclusion.length > 0) {
                query += glue + 'include=' + _inclusion.join();
                glue = '&';
            }
            const sort = []
            for (const field in _sortBy) {
                const desc = _sortBy[field];
                sort.push(`${desc ? '-' : ''}${field}`)
            }
            if (sort.length) {
                query += glue + 'sort=' + sort.join(',');
                glue = '&';
            }
            for (const param in _params) {
                query += glue + `${[param]}=${_params[param]}`
                glue = '&';
            }
            return query;
        }
    };
}


const relationshipProxyProcessor = <T extends Resource>(relationship: Relationship, client: Client): ToManyRelationship<T> & ToOneRelationship<T> => {
    let criteria = criteriaFactory<T>();
    let data: any = relationship.data as any;
    return {
        ...relationship,
        data,
        fields(type: string, fields: string[]) {
            criteria.fields(type, fields);
            return this;
        },
        filter(expression: Expression | string | null = null) {
            criteria.filter(expression);
            return this
        },
        include(pointer: string) {
            criteria.include(pointer);
            return this;
        },
        limitOffset(limit: number | null, offset: number | null) {
            criteria.limitOffset(limit, offset);
            return this;
        },
        numberSize(number: number | null, size: number | null) {
            criteria.numberSize(number, size);
            return this;
        },
        sort(field: string, desc: boolean = true) {
            criteria.sort(field, desc);
            return this;
        },
        param(name: string, value: string) {
            criteria.param(name, value);
            return this;
        },
        async self(signal?: AbortSignal) {
            if (relationship.links?.self) {
                const url = `${relationship.links.self}${criteria.toURL()}`
                const response = await client.get(url, signal);
                const doc = await response.json();
                criteria = criteriaFactory();
                return documentProcessor(doc, client);
            }
            criteria = criteriaFactory();
            return undefined;

        },
        async related(signal?: AbortSignal) {
            if (relationship.links?.related) {
                const url = `${relationship.links.related}${criteria.toURL()}`
                const response = await client.get(url, signal);
                criteria = criteriaFactory();
                const doc = await response.json();
                return documentProcessor(doc, client);
            }
            return undefined;
        },
        async update(identifiers: ResourceIdentifier | ResourceIdentifier[], signal?: AbortSignal) {
            if (relationship.links?.self) {
                const url = `${relationship.links.self}${criteria.toURL()}`
                const response = await client.patch(url, bodyFactory(identifiers), signal);
                criteria = criteriaFactory();
                if (response.status === HttpStatusCodes.NO_CONTENT) {
                    return null;
                }
                const doc = await response.json();
                return documentProcessor(doc, client);
            }
            return undefined;
        },
        async create(identifiers: ResourceIdentifier[], signal?: AbortSignal) {
            if (relationship.links?.self) {
                const url = `${relationship.links.self}${criteria.toURL()}`
                const response = await client.post(url, bodyFactory(identifiers), signal);
                criteria = criteriaFactory();
                if (response.status === HttpStatusCodes.NO_CONTENT) {
                    return null;
                }
                const doc = await response.json();
                return documentProcessor(doc, client);
            }
            return undefined;
        },
        async delete(identifiers: ResourceIdentifier[], signal?: AbortSignal) {
            if (relationship.links?.self) {
                const url = `${relationship.links.self}${criteria.toURL()}`
                const response = await client.delete(url, bodyFactory(identifiers), signal);
                criteria = criteriaFactory();
                if (response.status === HttpStatusCodes.NO_CONTENT) {
                    return null;
                }
                const doc = await response.json();
                return documentProcessor(doc, client);
            }
            return undefined;
        }
    }
}

const resourceProxyProcessor = <T extends Resource>(item: ResourceObject | ResourceIdentifier, client: Client): T | Identifier => {
    let criteria = criteriaFactory<T>();
    const self = "links" in item && item.links?.self ? item.links.self : undefined;
    if ("relationships" in item) {
        for (const name in item.relationships) {
            item.relationships[name] = relationshipProxyProcessor(item.relationships[name], client);
        }
    }
    return {
        ...item,
        fields(type: string, fields: string[]) {
            criteria.fields(type, fields);
            return this;
        },
        include(pointer: string) {
            criteria.include(pointer);
            return this;
        },
        param(name: string, value: string) {
            criteria.param(name, value);
            return this;
        },
        async self(signal?: AbortSignal) {
            if (self) {
                const url = `${self}${criteria.toURL()}`;
                const response = await client.get(url, signal);
                const doc = await response.json();
                criteria = criteriaFactory();
                return documentProcessor<T>(doc, client);
            }
            criteria = criteriaFactory();
            return undefined;
        },
        async update(data: PartialResource<T> | PartialResource<T>[], signal?: AbortSignal) {
            if (self) {
                const url = `${self}${criteria.toURL()}`;
                const response = await client.patch(url, bodyFactory(data), signal);
                criteria = criteriaFactory();
                if (response.status === HttpStatusCodes.NO_CONTENT) {
                    return null;
                }
                const doc = await response.json();
                return documentProcessor<T>(doc, client);
            }
            criteria = criteriaFactory();
            return undefined;
        },
        async delete(signal?: AbortSignal) {
            if (self) {
                const url = `${self}${criteria.toURL()}`;
                const response = await client.delete(url, undefined, signal);
                criteria = criteriaFactory();
                if (response.status === HttpStatusCodes.NO_CONTENT) {
                    return null;
                }
                const doc = await response.json();
                return documentProcessor<T>(doc, client);
            }
            criteria = criteriaFactory();
            return undefined;
        }
    }
}

const dataProcessor = <T extends Resource>(data: PrimaryData | undefined, client: Client): undefined | null | T | T[] | ResourceIdentifier | ResourceIdentifier[] => {
    if (data === undefined || data === null) {
        return data;
    }
    if (Array.isArray(data)) {
        return data.map((item) => {
            return resourceProxyProcessor<T>(item, client);
        })
    } else {
        return resourceProxyProcessor<T>(data, client);
    }
}

const inclusionProcessor = (resource: Resource, includedMap: Map<string, Resource>) => {
    if (!resource) return;
    if (Object.hasOwn(resource, 'hydrated')) return;
    Object.defineProperty(resource, 'hydrated', {value: true});
    if (resource.relationships) {
        for (const relationship of Object.values(resource.relationships)) {
            if (Array.isArray(relationship.data)) {
                relationship.data = relationship.data.map((identifier): Resource | ResourceIdentifier => {
                    const id = `${identifier.type}:${identifier.id}`;
                    if (includedMap.has(id)) {
                        const ref = includedMap.get(id) as Resource;
                        if (ref.relationships) {
                            inclusionProcessor(ref, includedMap);
                        }
                        return ref;
                    } else {
                        return identifier;
                    }
                })
            } else if (relationship.data) {
                const identifier = relationship.data as ResourceIdentifier;
                const id = `${identifier.type}:${identifier.id}`;
                if (includedMap.has(id)) {
                    const ref = includedMap.get(id) as Resource;
                    if (ref.relationships) {
                        inclusionProcessor(ref, includedMap);
                    }
                    relationship.data = ref;
                }
            }
        }
    }

}

const documentProcessor = <T extends Resource>(document: CompoundDocument, client: Client): ResourceDocument<T> & ResourceCollectionDocument<T> & IdentifierDocument<T> & IdentifierCollectionDocument<T> => {
    if (document.errors) {
        throw document
    }
    const includedMap = new Map<string, Resource>()
    if (document.included !== undefined) {
        document.included.forEach((resource) => {
            const proxy = dataProcessor(resource, client) as Resource;
            const id = `${resource.type}:${resource.id}`;
            includedMap.set(id, proxy);
        })
    }
    let data: any;
    if (Array.isArray(document.data)) {
        data = document.data.map(resource => {
            const proxy = dataProcessor(resource, client) as T;
            const id = `${resource.type}:${resource.id}`;
            includedMap.set(id, proxy);
            return proxy;
        }) as T[];
        data.forEach((resource: Resource) => {
            inclusionProcessor(resource, includedMap);
        })
    } else {
        data = dataProcessor(document.data, client) as T;
        const id = `${data.type}:${data.id}`;
        includedMap.set(id, data);
        inclusionProcessor(data, includedMap);
    }

    return {
        ...document, // <-- This! Right here, is cancer.
        data,
        async self(signal?: AbortSignal) {
            if (document.links?.self) {
                const url = `${document.links.self}`;
                const response = await client.get(url, signal);
                const doc: CompoundDocument = await response.json();
                return documentProcessor<T>(doc, client);
            }
            return undefined;
        },
        async related(signal?: AbortSignal) {
            if (document.links?.related) {
                const url = `${document.links.related}`;
                const response = await client.get(url, signal);
                const doc: CompoundDocument = await response.json();
                return documentProcessor<T>(doc, client);
            }
            return undefined;
        },
        async first(signal?: AbortSignal) {
            if (document.links?.first) {
                const url = `${document.links.self}`;
                const response = await client.get(url, signal);
                const doc: CompoundDocument = await response.json();
                return documentProcessor<T>(doc, client);
            }
            return undefined;
        },
        async last(signal?: AbortSignal) {
            if (document.links?.last) {
                const url = `${document.links.last}`;
                const response = await client.get(url, signal);
                const doc: CompoundDocument = await response.json();
                return documentProcessor<T>(doc, client);
            }
            return undefined;
        },
        async next(signal?: AbortSignal) {
            if (document.links?.next) {
                const url = `${document.links.next}`;
                const response = await client.get(url, signal);
                const doc: CompoundDocument = await response.json();
                return documentProcessor<T>(doc, client);
            }
            return undefined;
        },
        async prev(signal?: AbortSignal) {
            if (document.links?.prev) {
                const url = `${document.links.prev}`;
                const response = await client.get(url, signal);
                const doc: CompoundDocument = await response.json();
                return documentProcessor<T>(doc, client);
            }
            return undefined;
        }
    }


}

const useJsonApiWithoutIndex = function (client: Client = useNativeFetchClient()): SelectCollection {
    client.setHeader('Accept', 'application/vnd.api+json');
    client.setHeader('Content-Type', 'application/vnd.api+json');
    return {
        select<T extends Resource>(collection: string): CollectionMethods<T> {
            let criteria = criteriaFactory<T>();
            return {
                async create(data: PartialResource<T>, signal?: AbortSignal): Promise<ResourceDocument<T> | undefined | null> {
                    const url = `${collection}${criteria.toURL()}`;
                    const response = await client.post(url, bodyFactory(data), signal);
                    criteria = criteriaFactory();
                    if (response.status === HttpStatusCodes.NO_CONTENT) {
                        return null;
                    }
                    const doc: CompoundDocument = await response.json();
                    return documentProcessor<T>(doc, client);
                },
                fields(type: string, fields: string[]): CollectionMethods<T> {
                    criteria.fields(type, fields);
                    return this;
                },
                filter(expression: Expression | string | null = null): CollectionMethods<T> {
                    criteria.filter(expression);
                    return this;
                },
                id(id: string): ResourceMethods<T> {
                    return {
                        async self(signal?: AbortSignal): Promise<ResourceDocument<T>> {
                            const url = `${collection}/${id}${criteria.toURL()}`;
                            const response = await client.get(url, signal);
                            criteria = criteriaFactory();
                            const doc: CompoundDocument = await response.json();
                            return documentProcessor<T>(doc, client);
                        }
                    }

                },
                include(pointer: string): CollectionMethods<T> {
                    criteria.include(pointer);
                    return this;
                },
                limitOffset(limit: number | null, offset: number | null) {
                    criteria.limitOffset(limit, offset);
                    return this;
                },
                numberSize(number: number | null, size: number | null) {
                    criteria.numberSize(number, size);
                    return this;
                },
                sort(field: string, desc: boolean = true): CollectionMethods<T> {
                    criteria.sort(field, desc);
                    return this;
                },
                param(name: string, value: string): CollectionMethods<T> {
                    criteria.param(name, value);
                    return this;
                },
                async self(signal?: AbortSignal): Promise<ResourceCollectionDocument<T>> {
                    const url = `${collection}${criteria.toURL()}`;
                    const response = await client.get(url, signal);
                    criteria = criteriaFactory();
                    const doc: CompoundDocument = await response.json();
                    return documentProcessor<T>(doc, client);
                }
            }
        }
    }
}
const useJsonApiWithIndex = function <T extends Index>(index: IndexDocument<T>, client: Client = useNativeFetchClient()): ResourceList<T> {
    const ret = {};
    for (const type in index.links) {
        const link = index.links[type];
        if (link) {
            let collection: string;
            if (typeof link !== 'string') {
                collection = link.href
            } else {
                collection = link;
            }
            const {select} = useJsonApiWithoutIndex(client)
            Object.defineProperty(ret, type, {
                get: () => {
                    return select(collection)
                },
                configurable: true
            })
        }
    }
    return ret as ResourceList<T>;
}
//todo: swap args and set default value to client
const fetchIndexDocument = async <T extends Index>(client: Client, url: string): Promise<IndexDocument<T>> => {
    client.setHeader('Accept', 'application/vnd.api+json');
    client.setHeader('Content-Type', 'application/vnd.api+json');
    const res = await client.get(url);
    const doc: IndexDocument<T> = await res.json();
    if (doc.errors) {
        throw doc;
    }
    return doc;
}
export {
    //JSONAPI
    ResourceIdentifier,
    ResourceObject,
    Relationship,
    CompoundDocument,
    Meta,
    Link,
    //Interfaces
    Index,
    IndexDocument,
    Resource,
    ResourceList,
    ToManyRelationship,
    ToOneRelationship,
    PartialResource,
    SelectCollection,
    CollectionMethods,
    ResourceMethods,
    ResourceCollectionDocument,
    ResourceDocument,
    IdentifierDocument,
    IdentifierCollectionDocument,
    RecursivePartial,
    // Usage Strategy
    useJsonApiWithoutIndex,
    useJsonApiWithIndex,
    fetchIndexDocument,
    // Client
    Client,
    useNativeFetchClient,
    // Expression
    Expression,
    LiteralExpression,
    FieldExpression,
    BinaryExpression,
    FunctionExpression,
    GroupExpression,
    UnaryExpression,
    ExpressionStrategy,
    ODataExpressionStrategy,
    QuatrodotExpressionStrategy,
    ExpressionBuilder,
    PrettyExpressionBuilder,
    criteriaFactory
}
