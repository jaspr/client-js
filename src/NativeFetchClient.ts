import Client from "./Client";

export default function (baseURL = window.location.toString()): Client {

    const headers: Headers = new Headers();

    return {
        delete(url: string, body?: BodyInit | null, signal?: AbortSignal): Promise<Response> {
            const method = 'DELETE';
            const options: RequestInit = {headers, signal, method, body}
            const input = new URL(url, baseURL);
            return fetch(input, options);
        },

        get(url: string, signal?: AbortSignal): Promise<Response> {
            const method = 'GET';
            const options: RequestInit = {headers, signal, method}
            const input = new URL(url, baseURL);
            return fetch(input, options);
        },

        patch(url: string, body?: BodyInit | null, signal?: AbortSignal): Promise<Response> {
            const method = 'PATCH';
            const options: RequestInit = {headers, signal, method, body}
            const input = new URL(url, baseURL);
            return fetch(input, options);
        },

        post(url: string, body?: BodyInit | null, signal?: AbortSignal): Promise<Response> {
            const method = 'POST';
            const options: RequestInit = {headers, signal, method, body}
            const input = new URL(url, baseURL);
            return fetch(input, options);
        },

        setHeader(name: string, value: string): void {
            headers.set(name, value);
        }
    }

}
